/*
 * File      : application.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2006, RT-Thread Development Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */

/**
 * @addtogroup STM32
 */
/*@{*/

#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<rtthread.h>
#include	"task_msg.h"
#include	"AppMsg.h"
#include	"AppParam.h"

extern	void rt_thread_entry_Gui(void* parameter);
extern	void rt_thread_entry_Key(void* parameter);
extern	void rt_thread_entry_wtd(void* parameter);
extern	void rt_thread_entry_sample(void* parameter);
extern	void rt_thread_entry_business(void* parameter);

typedef struct {
	rt_thread_t		task;
	char			*name;
	void 		   (*ThreadFun)(void *parameter);
	void            *pstack;
	rt_uint32_t		pstackSize;
	rt_uint8_t      priority;
	rt_uint32_t     tick;
}TaskDef;



ALIGN(RT_ALIGN_SIZE)
static 	char 		thread_Gui_stack[2048];
struct 	rt_thread 	thread_Gui;

static 	char 		thread_Key_stack[256];
struct 	rt_thread 	thread_Key;

static 	char 		thread_stack_Wtd[256];
struct 	rt_thread 	thread_Wtd;

static 	char 		thread_stack_sample[256];
struct 	rt_thread 	thread_sample;

static 	char 		thread_stack_Business[512];
struct 	rt_thread 	thread_Business;


rt_mailbox_t	mailbox = RT_NULL;
rt_mq_t			queue 	= RT_NULL;


/*
 *	app thread def
 */
const	TaskDef	Task[] = {
{&thread_Business,	"Business",	rt_thread_entry_business,	thread_stack_Business,	512,	10,		5},	
{&thread_sample,	"sample",	rt_thread_entry_sample,		thread_stack_sample,	256,	10,		5},	
{&thread_Gui,		"Gui",		rt_thread_entry_Gui,		thread_Gui_stack,		2048,	11,		5},
{&thread_Key,		"Key",		rt_thread_entry_Key,		thread_Key_stack,		256,	11,		5},
{&thread_Wtd,		"Wtd",		rt_thread_entry_wtd,		thread_stack_Wtd,		256,	30,		5},
};
	

/*
 *	app thread init
 */
int rt_application_init()
{
	u32			i;
	rt_err_t	err;
	
	UserMsg_Init();		/* 初始化信号队列 	*/
	First_PowerTest();	/* 第一次上电判断 */
	Param_Init();		/* 初始化存储器 	*/
	
	for (i = 0; i < sizeof(Task) / sizeof(TaskDef); i++) {
		err = rt_thread_init(	Task[i].task, 
								Task[i].name, 
								Task[i].ThreadFun,
								RT_NULL,
								Task[i].pstack, 
								Task[i].pstackSize,
								Task[i].priority,
								Task[i].tick);
		if (err) {
			;
		}
		err = rt_thread_startup(Task[i].task);		
		if (err) {
			;
		}		
	}

    return 0;
}

/*@}*/
