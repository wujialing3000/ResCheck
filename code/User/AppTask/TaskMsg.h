

/*
 *	信号消息结构体定义
 */
typedef struct _MsgQ {
	void	*pMem;			/* 消息队列所指向的内存池	 */
	void	*pEvent;		/* 消息队列指针,指向消息队列 */
}MsgQ;

/*
 *	任务与任务之间传递的消息结构体
 */
typedef struct {
	u32		MsgType;			/*  */
	u32		wParam;				/*  */
	u32 	lParam;				/*  */
	void*	pVoid;				/*  */
}TaskMsgDef;





