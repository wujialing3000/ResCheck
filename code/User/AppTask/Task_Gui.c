

#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<rtthread.h>

#include 	<board.h>
#include	"bsp_led.h"

#include	"gdi.h"
#include	"guiwindows.h"


extern	CWindow  gWD_Emission;


void rt_thread_entry_Gui(void* parameter)
{
	GuiMsgInfo 	guiMsgInfo;					/* GUI消息结构实体 */	
	u8	 		err;
	
	BeginPaint();							/* 初始化GUI */
	EraseScreen();
	GuiMsgQueueCreate();					/* 创建GUI消息队列接收 */
	g_pCurWindow 		= &gWD_Emission;	/* 设置主窗口 */				
	guiMsgInfo.pWindow 	= g_pCurWindow;		/* 向主当前窗口发送加载消息 */
	guiMsgInfo.ID 		= WM_LOAD;
	GuiMsgQueuePost(&guiMsgInfo);
	while (1) {
		GuiMsgQueuePend(&guiMsgInfo, &err);	
		if (err == SYS_NO_ERR) {
			if ((g_pCurWindow != NULL) && (g_pCurWindow->ProcFunc != NULL)) {
				g_pCurWindow->ProcFunc(g_pCurWindow, &guiMsgInfo);
			}
		}
	}
}


