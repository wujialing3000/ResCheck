
#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<rtthread.h>

#include 	<board.h>
#include	"bsp_led.h"

#include	"gdi.h"
#include	"guiwindows.h"


extern	CWindow  gWD_Emission;

#define		DEFAULT_KEY		(0x0f)
#define		KEY_FILTER		(5)
#define		KEY_LONG_TIME	(100)
#define		KEY_REPEAT_TIME	(20)


typedef enum {
	Key_Press,					/* 按下有效 	*/
	key_Release,				/* 释放 		*/
	Key_LongPress,				/* 长按 		*/
	Key_Repeat,					/* 按键连发 	*/
	Key_Default_State = 0xFFFF,	/* 默认状态 	*/
}KeyStateEnumDef;


typedef struct {
	u32 Value;
	u32 State;
}KeyStructDef;


//const u32	KeyValue[10] = {};
/*
 *	按键扫描
 */
u32 Key_swap(void)
{
	u32 RowKey;
	
	
	RowKey = GPIO_ReadInputData(GPIOE);
	if (RowKey != 0xF000) {	
		return (RowKey>>12);
	}

	return DEFAULT_KEY;
}

/*
 *	按键处理
 */
u32 Button_Process(KeyStructDef *key)
{
	u32 	KeyValue;
	u32 	res = 0;
	static 	u32 	Count = 0;						/* 延时消抖 */
	static 	u32 	LongCnt = 0;					/* 长按计数 */
	static	u32		RepeatCnt = 0;					/* 连发计数 */	
	static 	u32 	state = 0;						/* 状态 */

	KeyValue = Key_swap();							/* 按键扫描 */
	if (KeyValue != DEFAULT_KEY) {
		if (Count < KEY_FILTER) {					/* 按下消抖 */
			Count++;
		} else {
			if (state == 0) {						/* 当前按键是否按下过 */
				state = 1;
				LongCnt = 0;
				key->Value = KeyValue;
				key->State = Key_Press;				/* 键按下 */
				res = 1;							/* 按键状态变化 */
			}
			if (LongCnt < KEY_LONG_TIME) {
				if (LongCnt++ >= KEY_LONG_TIME) {	/*  */
					key->Value = KeyValue;
					key->State = Key_LongPress;		/* 键长按 */
					res = 1;						/* 按键状态变化 */
				}			
			} else {
				if (RepeatCnt++ <= KEY_REPEAT_TIME) {
					RepeatCnt = 0;
					key->Value = KeyValue;
					key->State = Key_Repeat;			/* 键连发 */
					res = 1;						/* 按键状态变化 */	
				}
			}
		}
	} else {
		if (Count != 0) {							/* 弹起消抖 */
			Count--;
		} else {
			if (state == 1) {
				state = 0;					
				key->State = key_Release;	
				res = 1;						/* 按键状态变化 */
			}
		}
	}

	return res;
}

/*
 *	按键处理函数
 */
void rt_thread_entry_Key(void* parameter)
{
	KeyStructDef Key;
	
	Key.State = Key_Default_State;
	while(1) {
		rt_thread_delay( 1 );
		if (Button_Process(&Key)) {											/* 发生了状态变化才发送 */
			switch(Key.State) {
				case Key_Press:			/* 按下发送 */
					//Beep(1,5);		/* 按键音 */
					PostWindowMsg(&gWD_Emission, WM_KEYDOWN, Key.Value, 0);	/* 直接将变量中的数据放到队列中 */
					break;
				
				case Key_LongPress:		/* 长按发送 */
					
					break;
				
				case Key_Repeat:
					PostWindowMsg(&gWD_Emission, WM_KEYRepeat, Key.Value, 0);
					break;
				
				case key_Release:		/* 弹起发送 */
					break;
				
				default:
					break;
			}		
		
		}
	}
}

