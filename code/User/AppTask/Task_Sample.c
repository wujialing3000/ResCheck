#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<rtthread.h>
#include	"AppParam.h"
#include	"task_msg.h"
#include	"AppMsg.h"
#include	"bsp_cs5532.h"
#include	"bsp_fm24l64.h"

#include	"gdi.h"
#include	"guiwindows.h"


/*
 **********************************************************************************
 *								宏定义区
 **********************************************************************************
 */
#define		MAX_AVER_WIN		(2 + 2)
 
/*
 **********************************************************************************
 *								外部引用定义区
 **********************************************************************************
 */
extern	u8 CS5532_Flag(void);
extern	u8 fStepConfgAD_Ax( u8 u8Channel , u8 Step );
extern	s32 CS5532Ax_GetValue(void);
extern	CWindow  gWD_Emission;


/*
 **********************************************************************************
 *								全局变量定义区
 **********************************************************************************
 */
s32		AverWinBuff[MAX_AVER_WIN];
s32		Zero = 0;
s32		ad = 0;

/*
 **********************************************************************************
 *								函数定义区
 **********************************************************************************
 */
/*
 *	求平均值
 */
s32 Filter_Average(s32 num) 
{
	u32 		i;
	long long sum = 0;
	s32			Max;
	s32			Min;
	
	AverWinBuff[MAX_AVER_WIN - 1] = num;
	for (i = 0; i < MAX_AVER_WIN-1; i++) {
		AverWinBuff[i] = AverWinBuff[i + 1];
	}	
	Max = AverWinBuff[0];
	Min = AverWinBuff[0];
	for (i = 0; i < MAX_AVER_WIN; i++) {
		sum += AverWinBuff[i];
		if (Max < AverWinBuff[i]) {
			Max = AverWinBuff[i];
		}
		if (Min > AverWinBuff[i]) {
			Min = AverWinBuff[i];
		}
	}
	sum -= Max;
	sum -= Min;
	
	return sum/(MAX_AVER_WIN - 2);
}
/*
 *	
 */
void rt_thread_entry_sample(void* parameter)
{
	TaskMsgDef 	event_msg;
	
	fStepConfgAD_Ax(CS5532A1, 1);
	rt_thread_delay(2);
	fStepConfgAD_Ax(CS5532A1, 2);
	rt_thread_delay(20);
	while (1) {
		if (CS5532_Flag()) {
			ad = CS5532Ax_GetValue();
		} else {
			ad = 0;
		}
		ad = Filter_Average(ad);
		
		event_msg.MsgType 	= 0;
		event_msg.wParam 	= ad;
		event_msg.lParam 	= 0;
		event_msg.pVoid 	= 0;
		Task_QPost(&MsgQ_Business, &event_msg);
		
		rt_thread_delay(10);						/* 100ms产生一次 */
	}	
}
