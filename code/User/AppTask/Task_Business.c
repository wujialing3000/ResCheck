#include 	<stdio.h>
#include 	"string.h"
#include	"math.h"
#include 	"stm32f10x.h"
#include 	<rtthread.h>
#include	"AppParam.h"
#include	"task_msg.h"
#include	"AppMsg.h"
#include	"AppState.h"
#include	"common.h"
#include	"gdi.h"
#include	"guiwindows.h"

#include	"bsp_relay.h"

/*
 **********************************************************************************
 *								宏定义区
 **********************************************************************************
 */

#define		STABLE_TIME			(3)		/* 稳定判断次数 	*/
#define		STABLE_LIMIT		(0.2)	/* 普通稳定判断阀值 */
#define		ZERO_STABLE_LIMIT	(1)		/* 零位判断阀值 	*/

#define		ZR_TIME				(2)		/* 100ms的周期， */
#define		LOAD_TIME			(2)		
#define		CREEP_TIME			(2)
#define		UNLOAD_TIME			(2)		

#define		THRESHOLD			(Param.NetLup)

		
/*
 **********************************************************************************
 *								外部引用定义区
 **********************************************************************************
 */
extern	CWindow  gWD_Emission;
extern	CWindow  gWD_Celibration;

/*
 **********************************************************************************
 *								全局变量定义区
 **********************************************************************************
 */
static	float	last_zero;
static	float	last_creep;

static	u32		cnt;

/*
 **********************************************************************************
 *								函数定义区
 **********************************************************************************
 */
/*
 *	业务处理,状态机
 *	1.接入传感器
 *	2.检测零位,检测零漂
 *	3.判断当前是否大于灵敏度范围的50%，大于就是加载，判断蠕变
 *	4.判断是否进入零位范围，在零位范围就是卸载后，并判断回零值
 *	5.测试完成，等待下次测试
 */
void business_flow(float single, u8 ste) 
{
	u8		state;
	float	fbuf;
	
	ste = ste;
	
	state = AppState.BsnsFlow;
re:
	switch (state) {
		case BSNS_FLOW_START:
			cnt = 0;
			state = BSNS_ZERO_ING;										/* 一旦处于开始状态就进行零点检测 */
			goto re;
		
		case BSNS_ZERO_ING:
			if (cnt++ >= ZR_TIME) {
				if (Param.ZeroLdo <= single && single <= Param.ZeroLup) {	/* 零点判断 */
					cnt = 0;
					last_zero = single;
					state = BSNS_ZERO_OK;									/* 零点合格 */
				} else {					
					state = BSNS_ZERO_FAILE;								/* 零点失败 */
				}			
			}				
			break;					
							
		case BSNS_ZERO_OK:					
			state = BSNS_ZERO_DRIFT_ING;								/* 进行零点漂移测试 */
			goto re;					
				
		case BSNS_ZERO_DRIFT_ING:					
			if (cnt++ >= ZR_TIME) {										/* 每秒检测一个零漂 */					
				cnt = 0;
				if (fabs(single - last_zero) > fabs(Param.Float)) {		/* 零漂判断 */
					state = BSNS_ZERO_DRIFT_FAILE;						/* 零漂合格 */
				} else {
					state = BSNS_ZERO_DRIFT_OK;
//					last_zero 	 = single;
					Param.RtZero = single;
				}			
			}
			break;
			
		case BSNS_ZERO_DRIFT_OK:
			if ((single - last_zero) > PRESENT_CUT(Param.NetLdo + (Param.NetLup - Param.NetLdo)*0.5, 65)) {	/* 大于灵敏度范围的65%就认为加载成功 */
				cnt = 0;
				state = BSNS_SNSTV_ING;
			}
			break;

			

		case BSNS_SNSTV_ING:											/* 灵敏度处理 */
			if (cnt++ >= LOAD_TIME) {
				cnt = 0;
				fbuf = fabs(single - last_zero);
				if (Param.NetLdo <= fbuf && fbuf <= Param.NetLup) {
					Param.RtSensitive 	= fbuf;								/* 取灵敏度 */
					Param.RtGroup 		= Group_Cal(Param.RtSensitive - Param.NetLdo, 0, Param.Step);/* 计算档值 */
						
					last_creep = single;									/* 记录当前的值 */
					state = BSNS_SNSTV_OK;
				} else {
					state = BSNS_SNSTV_FAILE;
				}			
			}
			break;
			
		case BSNS_SNSTV_OK:												/* 灵敏度合格处理 */
			state = BSNS_CREEP_ING;
			goto re;
			
		
		case BSNS_CREEP_ING:
			if (cnt++ >= CREEP_TIME) {									/* 等待测试时间 */
				cnt = 0;
				if (fabs(single - last_creep) > fabs(Param.Creep)) {
					state = BSNS_CREEP_FAILE;							/* 不合格 */				
				} else {							
					state = BSNS_CREEP_OK;								/* 蠕变合格 */
				}
			}
			break;	

		case BSNS_CREEP_OK:
			if (Param.ZeroLdo <= single && single <= Param.ZeroLup) {	/* 判断是否再次进入零点范围内 */
				cnt = 0;
				state = BSNS_BKZR_ING;									/* 回零开始检测 */
			}	
			break;
			
		case BSNS_BKZR_ING:												/* 回零处理 */
			if (fabs(single - last_zero) < fabs((Param.RtSensitive) * 5/100)) {// 必须保证落回以前的零点范围
				if (cnt++ >= UNLOAD_TIME) {
					cnt = 0;
					fbuf = fabs(single - last_zero);
					if (fbuf < fabs(Param.Unload)) {
						Param.RtBkZero = fbuf;							/* 取回零值，当前零点 - 开始零点 */
						Param.FlowOkCnt++;
						state = BSNS_BKZR_OK;
					} else {
						Param.RtBkZero = fbuf;
						state = BSNS_BKZR_FAILE;
					}		
				}
			} else {
				state = BSNS_BKZR_ING;
			}
			break;
			
		case BSNS_BKZR_OK:
			Param.LastGroup 	= Param.RtGroup;						/* 上传档值 */	
			state = BSNS_FLOW_OK;
			break;
		
		
		
		/* 流程失败 */
		case BSNS_ZERO_FAILE:											/* 零点不合格 */
			AppState.BsnsFaile = BSNS_ZERO_FAILE;
			AppState.LastBsnsFaile = BSNS_ZERO_FAILE;
			state = BSNS_FLOW_FAILE;
			break;
		
		case BSNS_ZERO_DRIFT_FAILE:										/* 零漂不合格 */
			AppState.BsnsFaile = BSNS_ZERO_DRIFT_FAILE;
			AppState.LastBsnsFaile = BSNS_ZERO_DRIFT_FAILE;
			state = BSNS_FLOW_FAILE;
			break;	
		
		case BSNS_CREEP_FAILE:											/* 蠕变不合格 */
			AppState.BsnsFaile = BSNS_CREEP_FAILE;
			AppState.LastBsnsFaile = BSNS_CREEP_FAILE;
			state = BSNS_FLOW_FAILE;
			break;
		
		case BSNS_SNSTV_FAILE:											/* 灵敏度不合格 */
			AppState.BsnsFaile = BSNS_SNSTV_FAILE;
			AppState.LastBsnsFaile = BSNS_SNSTV_FAILE;
			state = BSNS_FLOW_FAILE;
			break;
		
		case BSNS_BKZR_FAILE:											/* 回零不合格 */
			AppState.BsnsFaile = BSNS_BKZR_FAILE;
			AppState.LastBsnsFaile = BSNS_BKZR_FAILE;
			state = BSNS_FLOW_FAILE;
			break;
		
		/* 流程结果 */

	}
	AppState.BsnsFlow = state;
}
/*
 *	
 */
void DataUnStable_Process(float single, u32 scnt)
{
	u8 state;
	
	state = AppState.BsnsFlow;
	switch (state) {
		case BSNS_FLOW_START:
		case BSNS_ZERO_ING:
		case BSNS_ZERO_DRIFT_ING:
			if (scnt >= (Param.ZrDriftTime*10+20)) {
				state += 2;
				AppState.BsnsFlow 		= BSNS_FLOW_FAILE;
				AppState.LastBsnsFaile 	= BSNS_ZERO_FAILE;
			}
			break;
		
		case BSNS_SNSTV_ING:
		case BSNS_CREEP_ING:
			if (scnt >= (Param.CreepTime*10+20)) {
				state += 2;
				AppState.BsnsFlow 		= BSNS_FLOW_FAILE;
				AppState.LastBsnsFaile 	= BSNS_SNSTV_FAILE;
			}
			break;
			
		case BSNS_CREEP_OK:
		case BSNS_BKZR_ING:
			if (scnt >= (Param.BkZrTime*10+20)) {
				state += 2;
				AppState.BsnsFlow 		= BSNS_FLOW_FAILE;
				AppState.LastBsnsFaile 	= BSNS_BKZR_FAILE;
				Param.RtBkZero = single - last_zero;
			}			
			break;
	}
		
}
/*
 *
 */
void State_DispValue(float single)
{
	u8 state;
	
	if (AppState.isSnsr == SNSR_OFF && AppState.isSnsrOK == SNSR_ON) {
		AppState.isSnsrOK = SNSR_OFF;
		PostWindowMsg(&gWD_Emission, WM_FLOW, 0, 0);		/* 刷新显示 */
		AppState.BsnsFlow 	 = BSNS_FLOW_WAIT;
		AppState.flowEndType = END_Force;
	}
	
	state = AppState.BsnsFlow;
	switch (state) {
		case BSNS_FLOW_START:
		case BSNS_ZERO_ING:
		case BSNS_ZERO_DRIFT_ING:
			Param.RtZero = single;
			break;
		
		case BSNS_ZERO_DRIFT_OK:
		case BSNS_SNSTV_ING:
		case BSNS_CREEP_ING:
			Param.RtSensitive = single - last_zero;					/* 显示灵敏度时是减掉零点 */
			break;

		
		case BSNS_FLOW_OK:
			if (AppState.isSnsr == SNSR_OFF) {						/* 检测已经取下现在传感器 */			
				state = BSNS_FLOW_WAIT;								/* 重新开始流程 */
				AppState.flowEndType = END_OK;
			} else {							
				state = BSNS_FLOW_OK;
			}
			Param.RtZero = single;
			break;
			
		case BSNS_FLOW_FAILE:
			if (AppState.isSnsr == SNSR_OFF) {						/* 检测已经取下现在传感器 */			
				state = BSNS_FLOW_WAIT;								/* 重新开始流程 */
				AppState.flowEndType = END_Faile;
			} else {							
				state = BSNS_FLOW_FAILE;
			}	
			Param.RtZero = single;
			break;			

		case BSNS_FLOW_WAIT:
			if (AppState.isSnsr == SNSR_ON) {
				state = BSNS_FLOW_START;
			}
			Param.RtZero = single;
			break;	
	}	
	AppState.BsnsFlow = state;
}
/*
 *	数据稳定
 */
u8 data_stable(float data)
{
	u8		res;
	static 	float	buff = 0;
	static	u32		stable_cnt;
	float	stable_limit;
	
	
	if (Param.ZeroLdo <= data && data <= Param.ZeroLup) {	/* 是否落入零点范围 */
		stable_limit = fabs(Param.Float * ZERO_STABLE_LIMIT);
	} else {
		stable_limit = fabs(buff * STABLE_LIMIT);	/* (当前值+零飘)*0.5 */
	}
	
	if (fabs(data - buff) <= stable_limit) {
		if (stable_cnt++ >= STABLE_TIME) {
			stable_cnt = STABLE_TIME;
			res = DATA_STABLE;
		}
	} else {
		stable_cnt = 0;
		res = DATA_UNSTABLE;
	}
	buff = data;
	
	return res;
}

/*
 *	数据处理
 */
float LinearProcess(s32 advalue) 
{
	float		gvale;
	
	gvale = advalue;
	gvale = gvale - (s32)Param.CaliZero;
	gvale = gvale * (Param.CaliVolt);
	gvale = gvale / ((s32)Param.CaliLoad - (s32)Param.CaliZero);

	return gvale;
}
/*
 *	业务任务
 */
void rt_thread_entry_business(void* parameter)
{
	TaskMsgDef 	event_msg = {0};
	s32			advalue;
	float		gvale;
	u8			ste;
	u32			scnt = 0;
	
	AppState.state		= SYS_RUN;				/* 实时运作状态 */
	AppState.BsnsFlow 	= BSNS_FLOW_START;		/* 流程等待开始 */
	AppState.TestSwitch = TEST_SWITCH_OFF;		/* 关测试 */
	while (1) {
		Task_QPend(&MsgQ_Business, &event_msg, sizeof(TaskMsgDef), RT_WAITING_FOREVER);
		advalue = (s32)event_msg.wParam;											/* 取采样任务发送过来的数据 */
		if (advalue >= 0x700000 || advalue <= -0x700000) {
			scnt = 0;							/* 断线重新开始计数 */
			cnt = 0;
			AppState.isSnsr = SNSR_OFF;			/* 断线 */
		} else {
			AppState.isSnsr = SNSR_ON;			/* 有传感器 */
			AppState.isSnsrOK = SNSR_ON;
		}
		advalue = advalue / 32;												/* 信号放大了64倍 */
		switch (AppState.state) {
			case SYS_RUN:
				gvale = LinearProcess(advalue);								/* 数据处理 */
				Param.RtWet = gvale;
				State_DispValue(gvale);
				ste = data_stable(gvale);
				if (AppState.isSnsr == SNSR_ON) {
					if (ste == DATA_STABLE) {
						scnt = 0;
						business_flow(gvale, ste);
					} else {
						cnt = 0;
						DataUnStable_Process(gvale, scnt++);
					}				
				} else {
					AppState.BsnsFlow = BSNS_FLOW_WAIT;
				}							
				PostWindowMsg(&gWD_Emission, WM_SHOW, 0, AppState.BsnsFlow);/* 刷新显示 */
				break;
			
			case SYS_CALIBRATION:
				PostWindowMsg(&gWD_Celibration, WM_SHOW, advalue, 0);		/* 发送ad码到标定界面 */
				break;
		}		
	}
}
