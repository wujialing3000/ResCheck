#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<rtthread.h>

#include 	<board.h>
#include	"bsp_led.h"



void rt_thread_entry_wtd(void* parameter)
{
	
	
	while (1) {
	
		rt_hw_led_on(0);
		rt_thread_delay( 25 );
		
		rt_hw_led_off(0);
		rt_thread_delay( 25 );
	}
	
	
}

