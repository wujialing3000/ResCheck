

#include 	<rtthread.h>
#include 	<stm32f10x.h>
#include	"bsp_key.h"




void rt_hw_init_key(void)
{
    GPIO_InitTypeDef	GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);

  	GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14 | GPIO_Pin_15;	
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_IPU;       
  	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
  	GPIO_Init(GPIOE, &GPIO_InitStructure);
}
