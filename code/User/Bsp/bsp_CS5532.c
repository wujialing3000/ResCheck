
#include "stm32f10x.h"
#include <stdio.h>

#include "bsp_cs5532.h"


/*
 **********************************************************************************
 *								宏定义区
 **********************************************************************************
 */
#define		SPI_X				SPI2

#define		CS5532_CS_HIGH()	GPIO_SetBits			(GPIOB, GPIO_Pin_12)
#define		CS5532_CS_LOW()		GPIO_ResetBits			(GPIOB, GPIO_Pin_12)
#define		CS5532_READ_MISO()	GPIO_ReadInputDataBit	(GPIOB, GPIO_Pin_14)

#define 	NS_50   6
#define 	CS5532_CS_Delay(x)   {	u8 i; \
									i = x;    \
									while(i--);}

/*
 **********************************************************************************
 *								外部引用定义区
 **********************************************************************************
 */
extern	u8 fStepConfgAD_Ax( u8 u8Channel , u8 Step );

/*
 **********************************************************************************
 *								全局变量定义区
 **********************************************************************************
 */									


/*
 **********************************************************************************
 *								函数定义区
 **********************************************************************************
 */									
/*
 *	cs5532 port and spi1 init
 */
void rt_hw_init_cs5532(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	SPI_InitTypeDef   SPI_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);

	//Configure SPI1 pins: SCK, MISO and MOSI
	GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF_PP;   //复用推挽输出
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_Out_PP; 	//推挽输出
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	SPI_InitStructure.SPI_Direction 		= SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode 				= SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize 			= SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL 				= SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA 				= SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS 				= SPI_NSS_Soft;   //SPI_NSS_Hard
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
	SPI_InitStructure.SPI_FirstBit 			= SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial 	= 7;
	SPI_Init(SPI_X, &SPI_InitStructure);

	// SPI2 enable
	SPI_Cmd(SPI_X, ENABLE);
}


unsigned char SPI_WriteByte(unsigned char data)
{
	unsigned char Data = 0;
	
	//Wait until the transmit buffer is empty
	while (SPI_I2S_GetFlagStatus(SPI_X, SPI_I2S_FLAG_TXE) == RESET) ;
	SPI_I2S_SendData(SPI_X, data);		// Send the byte
	
	//Wait until a data is received
	while(SPI_I2S_GetFlagStatus(SPI_X, SPI_I2S_FLAG_RXNE)==RESET);
	Data = SPI_I2S_ReceiveData(SPI_X);	// Get the received data
	
	return Data;	// Return the shifted data
}



static u8 CS5532A_SendByte(u8 byte)
{
	CS5532_CS_HIGH();
    CS5532_CS_LOW();
   
    CS5532_CS_Delay(NS_50);
    SPI_WriteByte(byte);
    CS5532_CS_Delay(NS_50);
	
	
    return 0;
}

static u8 CS5532A_SendWord(u32 uWord)
{
    CS5532_CS_HIGH();
	CS5532_CS_LOW();

    CS5532_CS_Delay(NS_50);

    SPI_WriteByte((uWord&0xff000000)>>24);
    SPI_WriteByte((uWord&0x00ff0000)>>16);
    SPI_WriteByte((uWord&0x0000ff00)>>8);
    SPI_WriteByte((uWord&0x000000ff)>>0);

    CS5532_CS_Delay(NS_50);
	
    return 0;
}

static u32 CS5532A_ReadWord(void)
{
    u32 uWord = 0;
	
	CS5532_CS_HIGH();
	CS5532_CS_LOW();
    CS5532_CS_Delay(NS_50);
    
    uWord  = (SPI_WriteByte(0x00)<<24);
    uWord |= (SPI_WriteByte(0x00)<<16);
    uWord |= (SPI_WriteByte(0x00)<<8);
    uWord |= (SPI_WriteByte(0x00)<<0);
    
    CS5532_CS_Delay(NS_50);
	
	
    return uWord;
}

static u32 CS5532Ax_ReadWord(void)
{
    u32 uWord = 0;
    
    uWord  = (SPI_WriteByte(0x00)<<16);
    uWord |= (SPI_WriteByte(0x00)<<8);
    uWord |= (SPI_WriteByte(0x00)<<0);
    SPI_WriteByte(0x00);				//读描述符，并舍弃
    
    return uWord;
}

/****************************************************************************
* 名	称：void CS5532_SetCMD(void)
* 功	能：设置成命令接收
* 入口参数：无
* 出口参数：无
* 说	明：	
****************************************************************************/
static void CS5532A_SetCMD(void)
{
	u8 i;
	
	for (i = 0; i<15; i++) {
		CS5532A_SendByte(SYNC1);
	}
	CS5532A_SendByte(SYNC0);		
}

/****************************************************************************
* 名	称：void CS5532_ConfigRegister(void)
* 功	能：配置寄存器
* 入口参数：无
* 出口参数：无
* 说	明：	
****************************************************************************/ 	
static void CS5532A_ConfigRegister(void)
{
	CS5532A_SendByte(ConfigRegister+WriteRegister);	//配置寄存器
	CS5532A_SendWord(SETUP1_VRS);					//参考电压, 2.5V < Vref <= VA+
}

/****************************************************************************
* 名	称：void CS5532_ChannelRegister(void)
* 功	能：通道寄存器
* 入口参数：无
* 出口参数：无
* 说	明：	
****************************************************************************/ 	
static void CS5532A_ChannelRegister(void)
{
	CS5532A_SendByte(ChannelRegister + ChannelSet_1 + WriteRegister);	//通道寄存器的通道配置寄存器1
	CS5532A_SendWord(SETUP1+SETUP2);									//写通道配置
}

/****************************************************************************
* 名	称：void CS5532_StartConvert(u8 flag,u8 SetupNum)
* 功	能：开始转换
* 入口参数：u8 flag			0单次转换	1连续转换
			u8 SetupNum 	使用第几个设置单元中的配置 1-8(CS5532 1-2)	
* 出口参数：无
* 说	明：无
****************************************************************************/
static u8 CS5532A_Start(u8 flag,u8 SetupNum)
{
	u8 SetupX = 0x00;

	switch(SetupNum)   //设置单元
	{
		case 1:
			SetupX=ChannelPointer_1;	//设置单元1
			break;
		case 2:
			SetupX=ChannelPointer_2;	//设置单元2
			break;
		case 3:
			SetupX=ChannelPointer_3;	//设置单元3
			break;
		case 4:
			SetupX=ChannelPointer_4;	//设置单元4
			break;
		case 5:
			SetupX=ChannelPointer_5;	//设置单元5
			break;
		case 6:
			SetupX=ChannelPointer_6;	//设置单元6
			break;
		case 7:
			SetupX=ChannelPointer_7;	//设置单元7
			break;
		case 8:
			SetupX=ChannelPointer_8;	//设置单元8
			break;
		default:
			SetupX=ChannelPointer_1;	//设置单元1
			break;	
	}
	if (flag == 0)
		CS5532A_SendByte(StartConvert+SetupX+Single);			//发送单次转换命令
	else
		CS5532A_SendByte(StartConvert+SetupX+Continuation);	//发送连续转换命令
	
	CS5532_CS_HIGH();

	return 1;
}

u8 CS5532_Flag(void)
{
	CS5532_CS_HIGH();
	CS5532_CS_LOW();	
	CS5532_CS_Delay(NS_50);
	if( Bit_RESET == CS5532_READ_MISO() )  //转换完成
		return 1;
	else
		return 0;
}
/*
 *
 */
s32 CS5532Ax_GetValue(void)
{
	s32 ADCData = 0;
	
	SPI_WriteByte(0x00);				  		//清SDO值
	ADCData = CS5532Ax_ReadWord();   			//读ADC数据

	//扩展
	if((ADCData & 0x00800000) != 0){
		ADCData |= 0xFF000000;
	}

	return ADCData;        			  //24位数据
}


/****************************************************************************
* 名	称：u8 fStepConfgAD_Ax( u8 u8Channel , u8 Step )
* 功	能：复位5532并初始化
* 入口参数：u8	要初始化的通道号
* 出口参数：u8	初始化步骤
* 说	明：
			第一次调用该函数 -》复位		Step = 1
			延时20ms(外部实现)
			第二次调用该函数 -》芯片配置	Step = 2
****************************************************************************/
u8 fStepConfgAD_Ax( u8 u8Channel , u8 Step )
{
	u8 ret=u8Channel;
    u32 temp;
	if( 1 == Step )		//复位
	{
		CS5532A_SetCMD();			   					 //设置成命令模式

		CS5532A_SendByte(ConfigRegister+WriteRegister);//对配置寄存器进行写操作
		CS5532A_SendWord(RS1);						 //复位,进入命令模式
		
		return 1 ;
	}
	//延时20毫秒后
	if( 2 == Step )		//配置
	{
		//对配置寄存器进行读操作	    0x10000000
		CS5532A_SendByte(ConfigRegister+ReadRegister);	    
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();					    //RV为1 0x10000000 复位操作结束	读取一次后RV自动为0
			if(temp!=0x10000000)
				ret &= ~CS5532A1;								    //复位标志不正确
		}
		if(!ret) {
			return ret;
		}
		
		//读配置寄存器  初始化值验证    0x00000000
		CS5532A_SendByte(ConfigRegister+ReadRegister);	    
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)                                   //配置寄存器复位默认值
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}
		
		//读偏移寄存器1  初始化值验证   0x00000000
		CS5532A_SendByte(ExcursionRegister+Excursion_1+ReadRegister);  
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)                                   //偏移寄存器1复位默认值
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}

		//读偏移寄存器2  初始化值验证   0x00000000
		CS5532A_SendByte(ExcursionRegister+Excursion_2+ReadRegister);
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)                                   //偏移寄存器2复位默认值
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}
		
		//读增益寄存器1  初始化值验证   0x01000000
		CS5532A_SendByte(GainRegister+GainSet_1+ReadRegister);	    
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x01000000)                                   //增益寄存器1复位默认值
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}

		//读增益寄存器2  初始化值验证   0x01000000
		CS5532A_SendByte(GainRegister+GainSet_2+ReadRegister);			
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x01000000)                                   //增益寄存器2复位默认值
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}

		//读通道寄存器1  初始化值验证   0x00000000
		CS5532A_SendByte(ChannelRegister+ChannelSet_1+ReadRegister);		
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}

		//读通道寄存器2  初始化值验证   0x00000000
		CS5532A_SendByte(ChannelRegister+ChannelSet_2+ReadRegister);		
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}

		//读通道寄存器3  初始化值验证   0x00000000
		CS5532A_SendByte(ChannelRegister+ChannelSet_3+ReadRegister);		
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)
				ret &= ~CS5532A1;
		}
		if(!ret) {
			return ret;
		}

		//读通道寄存器4  初始化值验证   0x00000000
		CS5532A_SendByte(ChannelRegister+ChannelSet_4+ReadRegister);		
		if(u8Channel & ret & CS5532A1) {
			temp = CS5532A_ReadWord();				        
			if(temp!=0x00000000)
				ret &= ~CS5532A1;
		}
	}
	
	CS5532A_ConfigRegister();	//设置,配置寄存器,参考电压
	CS5532A_ChannelRegister();	//设置,通道寄存器,物理通道 + 增益 + 字速率 + 单双极 + 锁存位 + 延时 +  开路检测 +  偏移/增益物理通道选择

	CS5532A_Start(1, 2);			//开启连续转换	
	
	return 1;
}

