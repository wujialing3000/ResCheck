

#ifndef _BSP_EEPROM_H_
#define _BSP_EEPROM_H_

#include "stm32f10x.h"

#define                EEPROM_PAGE_SIZE        256

#define                EEPROM_WRITE_ADDR                0XA0
#define                EEPROM_READ_ADDR                0XA1

#define                EEPROM_SCL_SET                GPIO_SetBits(GPIOB, GPIO_Pin_6)
#define         	   EEPROM_SCL_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_6)

#define                EEPROM_SDA_SET                GPIO_SetBits(GPIOB, GPIO_Pin_7)
#define         	   EEPROM_SDA_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_7)

#define        		   EEPROM_SDA_READ               GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_7)

#define                EEPROM_TRUE                    1
#define                EEPROM_FALSE                	0

extern void EEPROM_Init(void);
extern u8 	EEPROM_Write(u16 _addr, u8 *_pbyte, u16 _byte_count);
extern u8 	EEPROM_Read	(u16 _addr, u8 *_pbyte, u16 _byte_count);

#endif


extern	void rt_hw_init_fm24l64(void);

