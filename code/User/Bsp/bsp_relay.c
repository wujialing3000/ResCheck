
#include 	<rtthread.h>
#include 	<stm32f10x.h>
#include	"bsp_relay.h"



#define 	relay1_rcc                    RCC_APB2Periph_GPIOD
#define 	relay1_gpio                   GPIOD
#define 	relay1_pin                    GPIO_Pin_8


/*
 *	led port init
 */
void rt_hw_init_relay(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(relay1_rcc, ENABLE);

  	GPIO_InitStructure.GPIO_Pin 	= relay1_pin;	
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
  	GPIO_Init(relay1_gpio, &GPIO_InitStructure);
}


/*
 *	relay on
 */
void rt_hw_relay_on(rt_uint32_t n)
{
    switch (n) {
		case relay1:
			GPIO_SetBits(relay1_gpio, relay1_pin);
			break;

		default:
			break;
    }
}
/*
 *	relay off
 */
void rt_hw_relay_off(rt_uint32_t n)
{
    switch (n) {
		case relay1:
			GPIO_ResetBits(relay1_gpio, relay1_pin);
			break;

		default:
			break;
    }
}

#ifdef RT_USING_FINSH
#include <finsh.h>
void relay(rt_uint32_t relay, rt_uint32_t value)
{
		
	switch (value) {
		case 1:
			rt_hw_relay_on(relay);
			break;
		
		case 0:
			rt_hw_relay_off(relay);
			break;
		
	}

}
#endif
