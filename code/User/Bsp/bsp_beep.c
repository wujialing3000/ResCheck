
#include 	<rtthread.h>
#include 	<stm32f10x.h>

#include	"bsp_beep.h"


#define 	beep1_rcc                    RCC_APB2Periph_GPIOD
#define 	beep1_gpio                   GPIOD
#define 	beep1_pin                    GPIO_Pin_9

/*
 *	led port init
 */
void rt_hw_init_beep(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(beep1_rcc, ENABLE);

  	GPIO_InitStructure.GPIO_Pin 	= beep1_pin;	
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
  	GPIO_Init(beep1_gpio, &GPIO_InitStructure);
}

/*
 *	beep on
 */
void rt_hw_beep_on(rt_uint32_t n)
{
    switch (n) {
		case beep1:
			GPIO_SetBits(beep1_gpio, beep1_pin);
			break;

		default:
			break;
    }
}
/*
 *	beep off
 */
void rt_hw_beep_off(rt_uint32_t n)
{
    switch (n) {
		case beep1:
			GPIO_ResetBits(beep1_gpio, beep1_pin);
			break;

		default:
			break;
    }
}
