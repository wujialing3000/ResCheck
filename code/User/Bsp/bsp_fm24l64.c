
#include 	<rtthread.h>
#include 	<stm32f10x.h>

#include	"bsp_fm24l64.h"

#define IIC_SCK_PORT	GPIOB						//SCK
#define IIC_SCK_PIN		GPIO_Pin_6
#define IIC_SDA_PORT	GPIOB						//SDA
#define IIC_SDA_PIN		GPIO_Pin_7

void rt_hw_init_fm24l64(void)
{        
        GPIO_InitTypeDef  GPIO_InitStructure; 

        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

        GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

        GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void I2C_Delay(u32 nCount)
{
  while(nCount--);
}

static u8 IIC_Start(void)
{
        EEPROM_SDA_SET;
        EEPROM_SCL_SET;
        I2C_Delay(20);
        if(!EEPROM_SDA_READ)
                return EEPROM_FALSE;
        EEPROM_SDA_CLR;
		I2C_Delay(20);
        if(EEPROM_SDA_READ)
                return EEPROM_FALSE;
        EEPROM_SCL_CLR;
        return EEPROM_TRUE;
}

static void IIC_Stop(void)
{
        EEPROM_SCL_CLR;
        EEPROM_SDA_CLR;
		I2C_Delay(20);
        EEPROM_SCL_SET;
        EEPROM_SDA_SET;
}

static void IIC_ACK(void)
{
        EEPROM_SCL_CLR;
	I2C_Delay(20);
        EEPROM_SDA_CLR;
		I2C_Delay(20);
        EEPROM_SCL_SET;
	I2C_Delay(20);
        EEPROM_SCL_CLR;
	I2C_Delay(20);
}

static void IIC_NoACK(void)
{
        EEPROM_SCL_CLR;
	I2C_Delay(20);
        EEPROM_SDA_SET;
	I2C_Delay(20);
        EEPROM_SCL_SET;
	I2C_Delay(20);
        EEPROM_SCL_CLR;   
I2C_Delay(20);	
}

// 1 有ACK   0 无ACK
static u8 IIC_WaitACK(void)
{
        EEPROM_SCL_CLR;
        EEPROM_SDA_SET;
        EEPROM_SCL_SET;
        I2C_Delay(20);
        if(EEPROM_SDA_READ)
        {
                EEPROM_SCL_CLR;
                return EEPROM_FALSE;
        }
        EEPROM_SCL_CLR;
        return EEPROM_TRUE;
}

//数据从高位到地位
static void IIC_SendByte(u8 _byte)
{
        u8 i = 0;
        for (i=0; i<8; i++)
        {
                EEPROM_SCL_CLR;
                if (_byte&0x80)
                        EEPROM_SDA_SET;
                else
                        EEPROM_SDA_CLR;
                
                _byte <<= 1;
                EEPROM_SCL_SET;
        }
        EEPROM_SCL_CLR;
}

static u8 IIC_RecByte(void)
{
        u8 i = 0;
        u8 rec_byte;
        
        EEPROM_SDA_SET;
        for (i=0; i<8; i++)
        {
                rec_byte <<= 1;
                EEPROM_SCL_CLR;
                EEPROM_SCL_SET;
                if (EEPROM_SDA_READ)
                        rec_byte |= 0x01;
        }
        EEPROM_SCL_CLR;
        
        return rec_byte;
}
u8 EEPROM_Write(u16 _addr, u8 *_pbyte, u16 _byte_count)
{
        u8 addr_h = 0, addr_l = 0;
        u8 i;
        
        addr_h = _addr/EEPROM_PAGE_SIZE;
        addr_l = _addr%EEPROM_PAGE_SIZE;
        
        if (!IIC_Start())        return EEPROM_FALSE;
        IIC_SendByte(EEPROM_WRITE_ADDR);
        if(!IIC_WaitACK())
        {
                IIC_Stop();
                return EEPROM_FALSE;
        }
        IIC_SendByte(addr_h);
        IIC_WaitACK();
        IIC_SendByte(addr_l);
        IIC_WaitACK();
        
        for (i=0; i<_byte_count; i++)
        {
                IIC_SendByte(*(_pbyte+i));
                IIC_WaitACK();
        }
        IIC_Stop();
        
        return EEPROM_TRUE;
}

u8 EEPROM_Read(u16 _addr, u8 *_pbyte, u16 _byte_count)
{
        u8 addr_h = 0, addr_l = 0;
        u8 i;
        
        addr_h = _addr/EEPROM_PAGE_SIZE;
        addr_l = _addr%EEPROM_PAGE_SIZE;
        
        if (!IIC_Start())        return EEPROM_FALSE;
        IIC_SendByte(EEPROM_WRITE_ADDR);
        if(!IIC_WaitACK())
        {
                IIC_Stop();
                return EEPROM_FALSE;
        }
        IIC_SendByte(addr_h);
        IIC_WaitACK();
        IIC_SendByte(addr_l);
        IIC_WaitACK();
        
        IIC_Start();
        IIC_SendByte(EEPROM_READ_ADDR);
        IIC_WaitACK();
        
        for (i=0; i<_byte_count-1; i++)
        {
                *(_pbyte+i) = IIC_RecByte();
                IIC_ACK();
        }
        *(_pbyte+_byte_count-1) = IIC_RecByte();
        IIC_NoACK();
        IIC_Stop();
        
        return EEPROM_TRUE;
}



