
#include "stm32f10x.h"
#include "bsp_lcd.h"


#define	RS_rst		GPIO_ResetBits(GPIOC, GPIO_Pin_4)
#define	RS_set		GPIO_SetBits  (GPIOC, GPIO_Pin_4);

#define	RW_rst		GPIO_ResetBits(GPIOA, GPIO_Pin_7)
#define	RW_set		GPIO_SetBits  (GPIOA, GPIO_Pin_7);

#define CE_rst		GPIO_ResetBits(GPIOA, GPIO_Pin_6)
#define CE_set		GPIO_SetBits  (GPIOA, GPIO_Pin_6);

#define	CS1_rst		GPIO_ResetBits(GPIOA, GPIO_Pin_5)
#define	CS1_set		GPIO_SetBits  (GPIOA, GPIO_Pin_5);

#define	CS2_rst		GPIO_ResetBits(GPIOA, GPIO_Pin_4)
#define	CS2_set		GPIO_SetBits  (GPIOA, GPIO_Pin_4);

#define	CS3_rst		GPIO_ResetBits(GPIOA, GPIO_Pin_3)
#define	CS3_set		GPIO_SetBits  (GPIOA, GPIO_Pin_3);



/*
	PA3,PA4,PA5 - CS3,CS2,CS1
	
 */
void LCD_PortInit(void)
{
	GPIO_InitTypeDef 	GPIO_InitStructure;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);

  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 |GPIO_Pin_5 |
								  GPIO_Pin_6 | GPIO_Pin_7;	
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);

	
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;	
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOC, &GPIO_InitStructure);
		
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
								  GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;	
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOD, &GPIO_InitStructure);	
}

void delay(u32 num)
{
	while(num--) {
		;
	}
}

void LCD_WriteCmd(u8 content)
{
	delay(10);
	RS_rst;
	RW_rst;
	
	GPIO_Write(GPIOD, content);
	CE_set;
	CE_rst;
}

void LCD_WriteData(u8 content)
{
	delay(10);
	RS_set;
	RW_rst;
	
	GPIO_Write(GPIOD, content);
	CE_set;
	CE_rst;
}


void LCD_Write(u8 dat_com, u8 content)
{
	if (dat_com) {
		RS_set;
		RW_rst;
	} else {
		RS_rst;
		RW_rst;
	}
	GPIO_Write(GPIOD, content);
}


void wr_lcd_1(u8 dat_com, u8 content)
{
	CS1_rst;
	CS2_set;
	CS3_set;
	delay(6);
	LCD_Write(dat_com, content);
	CE_set;
	CE_rst;
}

void wr_lcd_2(u8 dat_com, u8 content)
{
	CS1_set;
	CS2_rst;
	CS3_set;
	delay(6);
	LCD_Write(dat_com, content);
	CE_set;
	CE_rst;
}

void wr_lcd_3(u8 dat_com, u8 content)
{
	CS1_set;
	CS2_set;
	CS3_rst;
	delay(6);
	LCD_Write(dat_com, content);
	CE_set;
	CE_rst;
}



void LCD_Initi(void)
{
	LCD_PortInit();

	wr_lcd_1(0,0xc0);
	wr_lcd_1(0,0x3f);
	wr_lcd_2(0,0xc0);
	wr_lcd_2(0,0x3f);
	wr_lcd_3(0,0xc0);
	wr_lcd_3(0,0x3f);
	
}

void ClearScreen(void)
{
	unsigned char j,k;
	for(k=0;k<8;k++)
	{
		for(j=0;j<64;j++)
		{
			wr_lcd_1(0,0xb8+k);
			wr_lcd_1(0,0x40+j);
			wr_lcd_1(1,0);
			wr_lcd_2(0,0xb8+k);
			wr_lcd_2(0,0x40+j);
			wr_lcd_2(1,0);
			wr_lcd_3(0,0xb8+k);
			wr_lcd_3(0,0x40+j);
			wr_lcd_3(1,0);
		}
	}
}


void LCD_WriteCommand(u8 cmd)
{

}

