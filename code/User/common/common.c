
#include	"common.h"

int	Group_Cal(float flup, float fldo, float fstep)
{
	int	lup;
	int	ldo;
	int step;
	
	if (flup < fldo) {
		return 0;
	}
	lup  = (int) (flup*10000 + 0.5);
	ldo  = (int) (fldo*10000 + 0.5);
	step = (int) (fstep*10000 + 0.5);
	
	return (lup - ldo)/step + 1;
}

