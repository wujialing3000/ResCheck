/*
 **********************************************************************************
 *								头文件区
 **********************************************************************************
 */
#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include	"TaskMsg.h"
#include	"AppMsg.h"
#include	"AppParam.h"
#include	"AppState.h"

#include	"bsp_fm24l64.h"

/*
 **********************************************************************************
 *								宏定义区
 **********************************************************************************
 */
 
/*
 **********************************************************************************
 *								外部引用定义区
 **********************************************************************************
 */
extern	u8 ParamOpera(void *param, u8 *buff);
extern	u8 ParamCheck(void *param, u8 *buff);

/*
 **********************************************************************************
 *								全局变量定义区
 **********************************************************************************
 */									
SysParamDef		Param;
AppStateDef		AppState;


/*
 *	每个参数的极限值
typedef struct {
	u8		ParamID;
	float	Ldo;
	float	Lup;
}ParamLimitDef;
 */
const	ParamLimitDef	ParamLimit[MAX_PARAM_NUM] = {
/*----------------------------------------------------------------------------------------------------------------------------------*/
/*ParamID			Ldo		Lup																											*/
/*----------------------------------------------------------------------------------------------------------------------------------*/	
{CC_CaliVolt,		0.5,	10},	
{CC_Second,			1,		100},		/*  */
{CC_DriftTime,		1,		30},		/*  */
{CC_CreepTime,		1,		30},		/*  */
{CC_BkZrTime,		1,		30},		/*  */
{CC_NetLdo,			0,		10},
{CC_NetLup,			0,		10},
{CC_Step,			0,		10},

{CC_Float,			0.001,		10},
{CC_Creep,			0.001,		10},
{CC_Unload,			0.001,		10},



};


/*
typedef struct {
	u8		ParamID;					//参数编号
	u8 		ParamAttr;					//bit0:只读 对于只读参数如果被写，接口线程直接返回错误
										//bit1:存盘 对于非存盘参数，直接发送到用户线程  
										//对于读参数命令，不用发送线程消息，直接从内存复制然后回应
	u8		DType;						//数据类型 
	u8		ParamLen;					//参数类型
	u8 		unuse;						//保留
	MsgQ	*pUser;						//
	void 	*ParamAddr;					//参数内存地址
	u32		DiskAddr;					//参数磁盘地址
	u8		(* CheckFun)(void *pParam);	//检查参数是否合法，由协议解析线程调用，如果检查非法，直接应答参数无效
	u8 		(* OperaFun)(void *pParam);	//参数变化引发的操作，有使用者线程调用
}ParamFormDef;
*/
const ParamFormDef	ParamForm[MAX_PARAM_NUM] = {
/*-----------------------------------------------------------------------------------------------------------------------------*/
/*ParamID		PAttr	DType	Len	*pUser	*ParamAddr			DiskAddr		(*CheckFun)	(*OperaFun)	   */
/*-----------------------------------------------------------------------------------------------------------------------------*/
{CC_CaliZero,	RW_CMD,	S32_Tp,	4,	NULL,	&Param.CaliZero,	PR_SysZero,		NULL,		ParamOpera	},
{CC_CaliLoad,	RW_CMD,	S32_Tp,	4,	NULL,	&Param.CaliLoad,	PR_SysLoad,		NULL,		ParamOpera	},
{CC_CaliVolt,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.CaliVolt,	PR_CaliVolt,	NULL,		ParamOpera	},
		
{CC_ZeroLdo	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.ZeroLdo,		PR_ZeroLdo,		NULL,		ParamOpera	},
{CC_ZeroLup	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.ZeroLup,		PR_ZeroLup,		NULL,		ParamOpera	},
{CC_NetLdo	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.NetLdo,		PR_SensLdo,		NULL,		ParamOpera	},
{CC_NetLup	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.NetLup,		PR_SensLup,		NULL,		ParamOpera	},
{CC_Step	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.Step,		PR_GearSize,	NULL,		ParamOpera	},
{CC_Group	,	RW_CMD,	S16_Tp,	2,	NULL,	&Param.Group,		PR_GearN,		NULL,		ParamOpera	},
{CC_Second	,	RW_CMD,	S16_Tp,	2,	NULL,	&Param.Second,		PR_TestTime,	ParamCheck,	ParamOpera	},
{CC_Float	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.Float,		PR_DriftRange,	NULL,		ParamOpera	},
{CC_Creep	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.Creep,		PR_CreepRange,	NULL,		ParamOpera	},
{CC_Unload	,	RW_CMD,	F32_Tp,	4,	NULL,	&Param.Unload,		PR_BkZeroRange,	NULL,		ParamOpera	},
	
{CC_DriftTime,	RW_CMD,	U32_Tp,	4,	NULL,	&Param.ZrDriftTime,	PR_LoadTime,	NULL,		ParamOpera	},
{CC_CreepTime,	RW_CMD,	U32_Tp,	4,	NULL,	&Param.CreepTime,	PR_UnLoadTime,	NULL,		ParamOpera	},
{CC_BkZrTime,	RW_CMD,	U32_Tp,	4,	NULL,	&Param.BkZrTime,	PR_Threshold,	NULL,		ParamOpera	},
{CC_BitNum,		RW_CMD,	U8_Tp,	1,	NULL,	&Param.bitNum,		PR_BitNum,		NULL,		ParamOpera	},
{CC_Factory,	RW_CMD,	U32_Tp,	4,	NULL,	&Param.factory,		PR_Factory,		NULL,		ParamOpera	},
};

const char *state_str[] = {
	"零点test"	,
	"零点ok"	,
	"零点fail"	,
	
	"零漂test"	,
	"零漂ok"	,
	"零漂fail"	,
	
	"蠕变test"	,
	"蠕变ok"	,
	"蠕变fail"	,

	"灵敏度test",
	"灵敏度ok"	,
	"灵敏度fail",
	
	"回零test"	,
	"回零ok"	,
	"回零fail"	,
	
	" ",
	"test ok",
	"test fail",
	"wait start",
};
/*
 **********************************************************************************
 *								函数定义区
 **********************************************************************************
 */	
/*
 *	参数初始化
 */
void Param_Init(void)
{
	u32		i;
	
	for (i = 0; i < MAX_PARAM_NUM; i++) {
		Param_OutSideOpera(i, PARAM_CMD_READ);	
	}
}


/*
 *	根据参数号找到指定参数极限值
 */
const ParamLimitDef * LimitCheck_Read_AccdID(u32 id)
{	
	u32 i;
	
	for (i = 0; i < MAX_PARAM_NUM; i++) {
		if (id == ParamLimit[i].ParamID) {
			return &ParamLimit[i];								/* 返回成功 */
		}
	}	
	return NULL;
}

/*
 *	通用操作
 */
u8 ParamOpera(void *param, u8 *buff)
{
	TaskParamMsgDef		*pmMsg = (TaskParamMsgDef *)param;	
	ParamFormDef const	*pmForm;
	u32					DkAddr = 0;	
	u8					res = 0;
	
	pmForm = &ParamForm[pmMsg->ParamID];
	if (pmForm == NULL) {
		return 1;
	}
	switch (pmForm->ParamLen) {
		case 1:
			DkAddr = GET_U8_OFFSET(pmForm->DiskAddr);
			break;
		case 2:
			DkAddr = GET_U16_OFFSET(pmForm->DiskAddr);
			break;
		case 4:
			DkAddr = GET_U32_OFFSET(pmForm->DiskAddr);
			break;
		default:
			DkAddr = pmForm->DiskAddr;
			break;
	}
	
	switch (pmMsg->CmdID) {
		case PARAM_CMD_READ:
			if (READ_(pmForm->ParamAttr)) {
				res = EEPROM_Read(DkAddr, (u8 *)pmMsg->value, pmMsg->Len);	
				if (!res) {
					res = 1;												/* 读取失败 */
				}
			} else {
				res = 1;
			}
			break;
		
		case PARAM_CMD_WRITE:
			if (WRITE_(pmForm->ParamAttr)) {
				res = EEPROM_Write(DkAddr, (u8 *)pmMsg->value, pmMsg->Len);
				if (!res) {
					res = 1;												/* 写入失败 */
				}
			} else {
				res = 1;
			}
			break;
	}
	
	return PARAM_OK;
}

/*
 *	通用判断操作
 */
u8 ParamCheck(void *param, u8 *buff)
{
	ParamFormDef 	const	*pmForm = (ParamFormDef *)param;
	ParamLimitDef 	const	*pmLimit;
	u8 						flag = 1;
	
	pmLimit = LimitCheck_Read_AccdID(pmForm->ParamID);
	switch (pmForm->ParamLen) {
		case 1:
			if (*(s8 *)buff < pmLimit->Ldo) {
				*(s8 *)buff = pmLimit->Ldo;
			}else if (*(u8 *)buff > pmLimit->Lup) {
				*(s8 *)buff = pmLimit->Lup;
			}		
			break;
			
		case 2:
			if (*(s16 *)buff < pmLimit->Ldo) {
				*(s16 *)buff = pmLimit->Ldo;
			}else if (*(u16 *)buff > pmLimit->Lup) {
				*(s16 *)buff = pmLimit->Lup;
			}			
			break;
			
		case 4:
			switch (pmForm->DType) {
				case S32_Tp:
				case U32_Tp:
					if (*(s32 *)buff < pmLimit->Ldo) {
						*(s32 *)buff = pmLimit->Ldo;
					}else if (*(u32 *)buff > pmLimit->Lup) {
						*(s32 *)buff = pmLimit->Lup;
					}						
					break;
				
				case F32_Tp:
					if (*(float *)buff < pmLimit->Ldo) {
						*(float *)buff = pmLimit->Ldo;
					}else if (*(float *)buff > pmLimit->Lup) {
						*(float *)buff = pmLimit->Lup;
					}	
					break;
				
			}
	
			break;
	
		default:
			
			break;
	}	
	
	return flag;
}

/*
 *	判断某个参数值的大小
 */
void ParamLimitCheck(u32 id)
{
	ParamFormDef const	*pmForm;
	
	pmForm = &ParamForm[id];
	if (pmForm->ParamAddr == NULL) {
		return ;
	}
	
	ParamCheck((void *)pmForm, pmForm->ParamAddr);
}
/*
 *	通用的内存和铁电读写操作
 */
u8 Param_OutSideOpera(u32 pId, u8 cmd)
{
	TaskParamMsgDef		param;
	TaskParamMsgDef		*pmMsg = &param;
	ParamFormDef const	*pmForm;
	u8					res = 0;
	
	memset(&param, 0, sizeof(param));
	
	pmForm = &ParamForm[pId];
	if (pmForm->OperaFun == NULL) {
		return 0;
	}
	if (pmForm->ParamAddr == NULL) {
		return 0;
	}
	if (pmForm->DiskAddr == NULL) {
		return 0;
	}	
	
	switch (cmd) {
		case PARAM_CMD_READ:
			param.ParamID 	= pId;
			param.CmdID 	= PARAM_CMD_READ;	
			param.Len		= pmForm->ParamLen;
			res = pmForm->OperaFun((void *)&param, NULL);
			if (res == PARAM_OK) {
				memcpy(pmForm->ParamAddr, pmMsg->value, param.Len);	
			}
				
			break;
		
		case PARAM_CMD_WRITE:
			param.ParamID 	= pId;
			param.CmdID 	= PARAM_CMD_WRITE;	
			param.Len		= pmForm->ParamLen;
			memcpy(pmMsg->value, pmForm->ParamAddr, param.Len);
			res = pmForm->OperaFun((void *)&param, NULL);	
			break;
	}
	
	return	res;
}
/*
 *	恢复出厂
 */
void Default_Factory(void)
{
	SysParamDef	DParam;
	u32	i;
	
	memset(&DParam, 0, sizeof(SysParamDef));
	DParam.ZeroLdo 	= -1;
	DParam.ZeroLup 	= 1;
	DParam.NetLdo 	= 0.9;
	DParam.NetLup 	= 1.3;
	DParam.Step 	= 0.005;
	
	DParam.Second 	= 3;
	DParam.Float 	= 0.002;
	DParam.Creep 	= 0.002;
	DParam.Unload 	= 0.002;
	
	DParam.CaliZero = 4879;
	DParam.CaliLoad = 10380;
	DParam.CaliVolt = 1.00;
	
	DParam.ZrDriftTime 	= 2;
	DParam.CreepTime 	= 3;
	DParam.BkZrTime 	= 3;
	
	DParam.factory = 0x55AA5AA5;
	
	Param = DParam;
	
	for (i = 0; i < MAX_PARAM_NUM; i++) {
		Param_OutSideOpera(i, PARAM_CMD_WRITE);
	}
}

void First_PowerTest(void)
{
	Param_OutSideOpera(CC_Factory, PARAM_CMD_READ);
	if (Param.factory != 0x55AA5AA5) {
		Default_Factory();
	}
}
/*
 **********************************************************************************
 *								end
 **********************************************************************************
 */
