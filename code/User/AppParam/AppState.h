
/*
 *	系统运行状态
 */
enum {
	SYS_RUN,				/* 运行状态 		*/
	SYS_CALIBRATION,		/* 标定状态 		*/
};

/*
 *	标定状态
 */
enum {
	CALIBRATION_NO,
	CALIBRATION_0mV,
	CALIBRATION_20mV,
};

/*
 *	测试开关
 */
enum {
	TEST_SWITCH_ON,
	TEST_SWITCH_OFF,
};
/*
 *	稳定状态
 */
enum {
	DATA_STABLE,
	DATA_UNSTABLE,
};
/*
 *	传感器状态
 */
enum {
	SNSR_ON,				/* 传感器接入 */
	SNSR_OFF,				/* 传感器未接入 */
};
/*
 *	 流程结束方式
 */
enum {
	END_OK,
	END_Faile,
	END_Force,				/* 强制结束，但是不一定是测试失败的地方 */
};
/*
 *	业务状态	
 */
enum {
	BSNS_ZERO_ING = 0,		/* 零点正在检测 	*/
	BSNS_ZERO_OK,
	BSNS_ZERO_FAILE,
	
	BSNS_ZERO_DRIFT_ING,	/* 零点漂移正在检测 */
	BSNS_ZERO_DRIFT_OK,
	BSNS_ZERO_DRIFT_FAILE,	
	
	BSNS_CREEP_ING,			/* 蠕变测试中 		*/
	BSNS_CREEP_OK,
	BSNS_CREEP_FAILE,
	
	BSNS_SNSTV_ING,			/* 灵敏度检测中 	*/
	BSNS_SNSTV_OK,			
	BSNS_SNSTV_FAILE,
	
	BSNS_BKZR_ING,			/* 回零测试中 		*/
	BSNS_BKZR_OK,
	BSNS_BKZR_FAILE,
	
	BSNS_FLOW_START,		/* 流程开始,上电后处于开始状态，运行中有重新接入，手动置入 		*/
	BSNS_FLOW_OK,			/* 整体流程通过 	*/
	BSNS_FLOW_FAILE,		/* 流程失败 		*/
	
	BSNS_FLOW_WAIT,			/* 等待开始 */
	
};

/*
 *	状态结构体
 */
typedef	struct {
	u8	state;				/* 实时运行还是标定状态 	*/
	u8	CaliFlow;			/* 标定流程 				*/
	u8	BsnsFlow;			/* 业务流程 				*/
	u8	BsnsFaile;			/* 记录测试失败的步骤 		*/
	u8	LastBsnsFaile;		/*  */
	u8	TestSwitch;			/* 测试开关 				*/
	u8	isSnsr;				/* 是否有传感器接入 		*/
	u8	isSnsrOK;	
	u8	flowEndType;		/* 流程结束方式 */
}AppStateDef;



extern	AppStateDef		AppState;

