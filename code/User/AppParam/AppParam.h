

#define		MAX_PARAM_NUM				20

#define		R_CMD						(0x01)
#define		W_CMD						(0x02)
#define		RW_CMD						(0x03)

#define		READ_(x)					(x & R_CMD)				/* 读允许判断 */
#define		WRITE_(x)					(x & W_CMD)				/* 写允许判断 */


#define		PARAM_CMD_READ				0
#define		PARAM_CMD_WRITE				1

#define		PARAM_OK					0
#define		PARAM_FALSE					1


typedef enum
{
	VOID_Tp				= 0,		//无类型，该类型没有对应的实际空间，即表示参数值长度的字段固定为0，而参数值字段始终没有
	S8_Tp				= 1,		//有符号8位整型
	S16_Tp				= 2,		//有符号16位整型
	S32_Tp				= 3,		//有符号32位整型
	U8_Tp				= 4,		//无符号8位整型
	U16_Tp				= 5,		//无符号16位整型
	U32_Tp				= 6,		//无符号32位整型
	F32_Tp				= 7,		//单精度浮点数
	CHAR_Array_Type		= 8,		//字符串
}Val_Type;							//值类型


/*
 *	每个参数的极限值
 */
typedef struct {
	u8		ParamID;
	float	Ldo;	
	float	Lup;
}ParamLimitDef;

/*
 *	参数操作表结构体
 */
typedef struct {
	u8		ParamID;
	u8 		ParamAttr;
	u8		DType;		/* 数据类型 */
	u8		ParamLen;	/* 长度 */
	void	*pUser;
	void 	*ParamAddr;
	u32		DiskAddr;
	u8		(* CheckFun)(void *pParam, u8 *buff);
	u8 		(* OperaFun)(void *pParam, u8 *buff);
}ParamFormDef;



/*
 *	参数id
 */
typedef enum {
	CC_CaliZero = 0,		/*  */
	CC_CaliLoad,
	CC_CaliVolt,			/* 标定的输入电压 */
	
	CC_ZeroLdo,				/* 零点范围 */
	CC_ZeroLup,
	CC_NetLdo,				/* 灵敏度范围 */
	CC_NetLup,
	CC_Step,				/* 分档值 */
	CC_Group,				/* 分档数 */
	CC_Second,				/* 测试时间 */
	CC_Float,				/* 零点漂移范围 */
	CC_Creep,				/* 蠕变合格范围 */
	CC_Unload,				/* 回零合格范围 */
	
	CC_DriftTime,
	CC_CreepTime,
	CC_BkZrTime,
	CC_BitNum,				/* 小数点显示位数 */
	CC_Factory,				/* 工厂值 */
}ParamIDDef;


/* 
 *	系统运行参数
 */
typedef struct {
	s32		CaliZero;
	s32		CaliLoad;
	float	CaliVolt;					
	
	float	ZeroLdo;					/* 零点范围 */	
	float	ZeroLup;
	float	NetLdo;						/* 灵敏度范围 */
	float	NetLup;
	float	Step;						/* 分档值 */
	s16		Group;						/* 分档数 */
	s16		Second;	
	float	Float;						/* 零漂范围 */
	float	Creep;						/* 蠕变范围 */
	float	Unload;						/* 回零范围，卸载后的零点范围 */
	
	float	RtWet;
	float	RtZero;						/* 零点 */
	float	RtSensitive;				/* 灵敏度 */
	float	RtBkZero;					/* 回零值 */
	u16		RtGroup;					/* 当前分档 */
	float	LastGroup;					/* 上次分档 */
	u32		FlowOkCnt;					/* 流程成功计数 */
	
	u32		ZrDriftTime;				/* 零漂时间 */
	u32		CreepTime;					/* 蠕变时间 */	
	u32		BkZrTime;					/* 回零时间 */
	
	u32		factory;
	u8		bitNum;
}SysParamDef;




#define U8_PARAM_BASE 			0		//单字节区域, 定义64字节长度
#define U16_PARAM_BASE			64		//2字节区域, 定义128字节长度
#define F32_PARAM_BASE			192		//浮点参数区域, 定义256字节长度
#define U32_PARAM_BASE			448	    //整型参数区域，定义256字节长度
#define USR_PARAM_BASE			704


/*
 *	参数在铁电中的存储地址
 */
enum {
/* 1字节区域, 最大存储64个参数 */
	U8_RE = U8_PARAM_BASE,
/* 2字节区域, 最大存储64个参数 */	
	U16_RE = U16_PARAM_BASE,
	PR_GearN,
	PR_TestTime,
	PR_BitNum,
	
/* 4字节区域, 最大存储128个参数,u32和f32放在一块区域 */
	U32_RE = F32_PARAM_BASE,
	
	PR_SysZero,
	PR_SysLoad,
	PR_SNSRVOLT,
	PR_ZeroLdo,
	PR_ZeroLup,
	PR_SensLdo,
	PR_SensLup,
	PR_GearSize,
	PR_DriftRange,
	PR_CreepRange,
	PR_BkZeroRange,
	PR_CaliVolt,
	PR_LoadTime,
	PR_UnLoadTime,
	PR_Threshold,
	
	PR_Factory,
	
/* 其他参数区域, 起始地址704 */
	
	
};

#define GET_U8_OFFSET(PARAMX)  	(U8_PARAM_BASE +    PARAMX - U8_PARAM_BASE)
#define GET_U16_OFFSET(PARAMX) 	(U16_PARAM_BASE + ((PARAMX - U16_PARAM_BASE)<<1))
#define GET_U32_OFFSET(PARAMX) 	(F32_PARAM_BASE + ((PARAMX - F32_PARAM_BASE)<<2))
#define GET_F32_OFFSET(PARAMX) 	(F32_PARAM_BASE + ((PARAMX - F32_PARAM_BASE)<<2))


extern	SysParamDef		Param;
extern	const char *state_str[];

extern	void 	Param_Init(void);
extern	u8 		Param_OutSideOpera(u32 pId, u8 cmd);
extern	void 	ParamLimitCheck(u32 id);
extern	void 	First_PowerTest(void);
