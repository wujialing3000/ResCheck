#include 	<stdio.h>
#include 	<string.h>
#include 	<stdlib.h>
#include	<math.h>

#include 	"stm32f10x.h"
#include 	<board.h>

#include	"AppParam.h"

#include	"gdi.h"
#include	"guiwindows.h"
#include 	"TextEdit.h"


#include 	"bsp_key.h"



extern	CWindow  gWD_TestFun;

static void Form_TestFun_Timer(void *ptmr);
static void Form_TestFun_Draw(LPWindow pWindow);
static void Form_TestFun_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo);

float	Second = 1.8765;
static	char	sSecond[10];


DEF_TEXT_LABEL(msZeroDriftRange,&gWD_TestFun, 0,  0,  32, 16, CTRL_VISABLE, "��Ư��Χ:");
DEF_TEXT_LABEL(msCreepRange, 	&gWD_TestFun, 0, 16,  32, 16, CTRL_VISABLE, "��䷶Χ:");
DEF_TEXT_LABEL(msBkZeroRange,	&gWD_TestFun, 0, 32,  32, 16, CTRL_VISABLE, "���㷶Χ:");


DEF_SMALL_FLOAT_LABLE(mFloat,  &gWD_TestFun, 63,   0, 42, 14, CTRL_VISABLE, &Param.Float, 4, -9999, 9999, "", TA_LEFT);
DEF_SMALL_FLOAT_LABLE(mCreep,  &gWD_TestFun, 63,  16, 42, 14, CTRL_VISABLE, &Param.Creep, 4, -9999, 9999, "", TA_LEFT);
DEF_SMALL_FLOAT_LABLE(mUnload, &gWD_TestFun, 63,  32, 42, 14, CTRL_VISABLE, &Param.Unload, 4, -9999, 9999, "", TA_LEFT);

static LPControl marrLPControl[] = 
{
	&msZeroDriftRange,
	&msCreepRange,
	&msBkZeroRange,
	
	&mFloat,
	&mCreep,
	&mUnload,
	
};


CWindow  gWD_TestFun = {
    marrLPControl,
	sizeof(marrLPControl)/sizeof(LPControl),
    0,
    0,
    0,
    192,
    64,
	WS_VISABLE,
    NULL,
    10,
    NULL,
    Form_TestFun_Timer,
    Form_TestFun_Draw,
    Form_TestFun_Proc
};


void Form_TestFun_Timer(void *ptmr)
{
	LPWindow lpWindow = (LPWindow)ptmr;
	
	if (lpWindow != NULL) {			

	}
}

void Form_TestFun_Draw(LPWindow pWindow)
{
	u16 		i;
	LPControl 	lpControl;				/* ���ӿؼ��ṹָ�� */

    SetColor(0);
	EraseBuffer();						//����Դ�
	SetRedraw(FALSE);					//��ֹ��ͼ	m_nRedrawIndex++;
	EnableScreenFlush(FALSE);			//��ֹˢ��	m_bScreenFlush = FALSE;
	SetGdiView(	pWindow->nViewPosX, 
				pWindow->nViewPosY, 
				pWindow->nViewSizeX, 
				pWindow->nViewSizeY);	//������ͼ
	EnableGdiView(TRUE);				//			m_bUseView = TRUE

	//���ƿؼ�
	for (i = 0; i < pWindow->nNbControls; i++) {
		lpControl = *(pWindow->pLPControls + i);	/* ȡ��ǰ�ؼ� */
		if (lpControl->state & CTRL_VISABLE) {
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
			lpControl->DrawFunc(lpControl);
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
		}
	}
	EnableScreenFlush(TRUE);			//ʹ��ˢ��
	FlushScreen();						//ˢ��
	SetRedraw(TRUE);					//ʹ�ܻ�ͼ
}


void Form_TestFun_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo)
{
	char	cTemp[20];

	switch (pGuiMsgInfo->ID) {
		case WM_LOAD:
			mFloat.state  |= (CTRL_FOCUS);
			mCreep.state  &= (~CTRL_FOCUS);
			mUnload.state &= (~CTRL_FOCUS);

		case WM_SHOW:
			sprintf(sSecond, "%3dS", Param.Second);
			pWindow->DrawFunc(pWindow);
			break;
		
		case WM_FLOW:
			pWindow->DrawFunc(pWindow);			
			break;

		case WM_KEYRepeat:
		case WM_KEYDOWN: 
			pWindow->DrawFunc(pWindow);
			switch(pGuiMsgInfo->wParam) {	//����ֵ����
				case KEY_OK:
					memset(cTemp, 0, sizeof(cTemp));
					if(mFloat.state & CTRL_FOCUS) {
						if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 6)) {
							Param.Float = atof(cTemp);
							ParamLimitCheck(CC_Float);
							Param_OutSideOpera(CC_Float, PARAM_CMD_WRITE);
						}
                    } else if(mCreep.state & CTRL_FOCUS) {
						if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 6)) {
							Param.Creep = atof(cTemp);
							ParamLimitCheck(CC_Creep);
							Param_OutSideOpera(CC_Creep, PARAM_CMD_WRITE);
						}
                    } else if(mUnload.state & CTRL_FOCUS) {
						if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 6)) {
							Param.Unload = atof(cTemp);
							ParamLimitCheck(CC_Unload);
							Param_OutSideOpera(CC_Unload, PARAM_CMD_WRITE);
						}
                    } 
					break;	
				
				case KEY_LEFT:
					if (mUnload.state & CTRL_FOCUS) {
						mUnload.state &=~CTRL_FOCUS;
						mCreep.state |= CTRL_FOCUS;
					} else if (mCreep.state & CTRL_FOCUS) {
						mCreep.state &=~CTRL_FOCUS;
						mFloat.state |= CTRL_FOCUS;
					} else if (mFloat.state & CTRL_FOCUS) {
                        mFloat.state &=~CTRL_FOCUS;
                        mUnload.state |= CTRL_FOCUS;
                    }	
					break;
				
				case KEY_RIGHT:
					if (mFloat.state & CTRL_FOCUS) {
                        mFloat.state &=~CTRL_FOCUS;
                        mCreep.state |= CTRL_FOCUS;
                    } else if (mCreep.state & CTRL_FOCUS) {
                        mCreep.state &=~CTRL_FOCUS;
                        mUnload.state |= CTRL_FOCUS;
                    } else if (mUnload.state & CTRL_FOCUS) {
                        mUnload.state &=~CTRL_FOCUS;
                        mFloat.state |= CTRL_FOCUS;
                    }
					break;
				
				case KEY_BACK:
					if (pWindow->pParentWindow != NULL) {
						g_pCurWindow = pWindow->pParentWindow;
						PostWindowMsg(g_pCurWindow, WM_LOAD,0,0);
					}
					break;
				
				default: 
					break;
			}
			break;

		default:			
			break;		

	}
}

