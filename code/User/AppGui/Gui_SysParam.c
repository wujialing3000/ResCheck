#include 	<stdio.h>
#include 	<string.h>
#include 	<stdlib.h>
#include	<math.h>

#include 	"stm32f10x.h"
#include 	<board.h>

#include	"AppParam.h"

#include	"gdi.h"
#include	"guiwindows.h"
#include 	"TextEdit.h"
#include 	"bsp_key.h"


extern	CWindow  	gWD_SysParam;

static void Form_SysParam_Timer(void *ptmr);
static void Form_SysParam_Draw(LPWindow pWindow);
static void Form_SysParam_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo);


static	char buff[3][20];


DEF_TEXT_LABEL(msLoad,			&gWD_SysParam, 0,   0,  32, 16, CTRL_VISABLE, "零漂时间:");
DEF_TEXT_LABEL(msUnLoad,		&gWD_SysParam, 0,  16,  32, 16, CTRL_VISABLE, "蠕变时间:");
DEF_TEXT_LABEL(msThreshold,		&gWD_SysParam, 0,  32,  32, 16, CTRL_VISABLE, "回零时间:");
	
DEF_TEXT_LABEL(mgLoad,			&gWD_SysParam, 63,  0, 28, 14, CTRL_VISABLE, buff[0]);
DEF_TEXT_LABEL(mgUnLoad,		&gWD_SysParam, 63, 16, 28, 14, CTRL_VISABLE, buff[1]);
DEF_TEXT_LABEL(mgThreshold,		&gWD_SysParam, 63, 32, 28, 14, CTRL_VISABLE, buff[2]);


static LPControl marrLPControl[] = 
{
	&msLoad,		
	&msUnLoad,		
	&msThreshold,	
	
	&mgLoad,		
	&mgUnLoad,	
	&mgThreshold,
};

CWindow  gWD_SysParam = {
    marrLPControl,
	sizeof(marrLPControl)/sizeof(LPControl),
    0,
    0,
    0,
    192,
    64,
	WS_VISABLE,
    NULL,
    10,
    NULL,
    Form_SysParam_Timer,
    Form_SysParam_Draw,
    Form_SysParam_Proc
};

void Form_SysParam_Timer(void *ptmr)
{
	LPWindow lpWindow = (LPWindow)ptmr;
	
	if (lpWindow != NULL) {			

	}
}


void Form_SysParam_Draw(LPWindow pWindow)
{
	u16 		i;
	LPControl 	lpControl;							/* 子子控件结构指针 */
			
    SetColor(0);			
	EraseBuffer();									/* 清除显存 */
	SetRedraw(FALSE);								/* 禁止绘图	m_nRedrawIndex++; */
	EnableScreenFlush(FALSE);						/* 禁止刷屏	m_bScreenFlush = FALSE; */
	SetGdiView(	pWindow->nViewPosX, 			
				pWindow->nViewPosY, 			
				pWindow->nViewSizeX, 			
				pWindow->nViewSizeY);				/* 设置视图 */
	EnableGdiView(TRUE);							/* m_bUseView = TRUE */			

	for (i = 0; i < pWindow->nNbControls; i++) {	/* 绘制控件 */
		lpControl = *(pWindow->pLPControls + i);	/* 取当前控件 */
		if (lpControl->state & CTRL_VISABLE) {
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
			lpControl->DrawFunc(lpControl);
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
		}
	}
	EnableScreenFlush(TRUE);						/* 使能刷屏 */
	FlushScreen();									/* 刷屏 */
	SetRedraw(TRUE);								/* 使能绘图 */
}

void Form_SysParam_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo)
{
	CControl* 	pControl;
	char		cTemp[20];
	
	switch (pGuiMsgInfo->ID) {
		case WM_LOAD:
			mgLoad.state  |= (CTRL_FOCUS);
			mgUnLoad.state &= (~CTRL_FOCUS);
			mgThreshold.state  &= (~CTRL_FOCUS);

		case WM_SHOW:	
			sprintf(buff[0], "%3dS", Param.ZrDriftTime);
			sprintf(buff[1], "%3dS", Param.CreepTime);
			sprintf(buff[2], "%3dS", Param.BkZrTime);
			pWindow->DrawFunc(pWindow);
			break;
		
		case WM_FLOW:
			pWindow->DrawFunc(pWindow);			
			break;

		case WM_UPDATECTRL:
			pControl = (CControl *)(pGuiMsgInfo->wParam);
			if (pControl != NULL) {
				pControl->DrawFunc(pControl);				/* 显示 */
			} 			
			break;

		case WM_TIMEUPDATE:
// 			PostWindowMsg(pWindow, WM_UPDATECTRL, (uint32)&gStatusBar, 0);
			break;
		
		case WM_KEYRepeat:
		case WM_KEYDOWN: 
			pWindow->DrawFunc(pWindow);
			switch(pGuiMsgInfo->wParam) {	//按键值处理
				case KEY_OK:
					memset(cTemp, 0, sizeof(cTemp));
					if (mgLoad.state & CTRL_FOCUS) {
                        if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 3)) {
							Param.ZrDriftTime = atoi(cTemp);
							ParamLimitCheck(CC_DriftTime);
							Param_OutSideOpera(CC_DriftTime, PARAM_CMD_WRITE);
						}
                    } else if (mgUnLoad.state & CTRL_FOCUS) {
                        if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 3)) {
							Param.CreepTime = atoi(cTemp);
							ParamLimitCheck(CC_CreepTime);
							Param_OutSideOpera(CC_CreepTime, PARAM_CMD_WRITE);
						}
                    } else if (mgThreshold.state & CTRL_FOCUS) {
                       if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 3)) {
							Param.BkZrTime = atoi(cTemp);
							ParamLimitCheck(CC_BkZrTime);
							Param_OutSideOpera(CC_BkZrTime, PARAM_CMD_WRITE);
					   }
                    }
					break;	
				
				case KEY_LEFT:
					if (mgThreshold.state & CTRL_FOCUS) {
                        mgThreshold.state &=~CTRL_FOCUS;
                        mgUnLoad.state |= CTRL_FOCUS;
                    } else if (mgUnLoad.state & CTRL_FOCUS) {
                        mgUnLoad.state &=~CTRL_FOCUS;
                        mgLoad.state |= CTRL_FOCUS;
                    } else if (mgLoad.state & CTRL_FOCUS) {
                        mgLoad.state &=~CTRL_FOCUS;
                        mgThreshold.state |= CTRL_FOCUS;
                    }
					break;
				
				case KEY_RIGHT:
					if (mgLoad.state & CTRL_FOCUS) {
                        mgLoad.state &=~CTRL_FOCUS;
                        mgUnLoad.state |= CTRL_FOCUS;
                    } else if (mgUnLoad.state & CTRL_FOCUS) {
                        mgUnLoad.state &=~CTRL_FOCUS;
                        mgThreshold.state |= CTRL_FOCUS;
                    } else if (mgThreshold.state & CTRL_FOCUS) {
                        mgThreshold.state &=~CTRL_FOCUS;
                        mgLoad.state |= CTRL_FOCUS;
                    }
					break;
				
				case KEY_BACK:
					Param_OutSideOpera(CC_DriftTime, 	PARAM_CMD_READ);
					Param_OutSideOpera(CC_CreepTime, 	PARAM_CMD_READ);
					Param_OutSideOpera(CC_BkZrTime, 	PARAM_CMD_READ);
					if (pWindow->pParentWindow != NULL) {
						g_pCurWindow = pWindow->pParentWindow;
						PostWindowMsg(g_pCurWindow, WM_LOAD, 0, 0);
					}
					break;
					
				default: 
					break;
			}
			break;

		default:			
			break;		

	}
}

