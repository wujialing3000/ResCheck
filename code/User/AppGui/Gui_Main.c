#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<board.h>
#include 	<rtthread.h>
#include	"AppParam.h"
#include	"AppState.h"

#include	"gdi.h"
#include	"guiwindows.h"
#include 	"guiwindows.h"
#include 	"bsp_key.h"

extern	CWindow  	gWD_Emission;
extern	CWindow		gWD_MainMenu;		//菜单界面

static void Form_Emission_Timer(void *ptmr);
static void Form_Emission_Draw(LPWindow pWindow);
static void Form_Emission_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo);

extern	s32		Zero;
extern	s32		ad;
static 	char	StrBuff[4][30];
static	u8		tim_flag = 0;
static	u8		flash_flag = 0;


DEF_TEXT_LABEL(mZero,   	 &gWD_Emission,  0,  0,  32, 16, CTRL_VISABLE, "零  点:");
DEF_TEXT_LABEL(mOnly,   	 &gWD_Emission,  0, 16,  32, 16, CTRL_VISABLE, "灵敏度:");
DEF_TEXT_LABEL(mbkzero,   	 &gWD_Emission,  0, 32,  32, 16, CTRL_VISABLE, "回  零:");

DEF_TEXT_LABEL(mZeroOk,   	 &gWD_Emission,  115, 0,  14, 14, 0, "OK");
DEF_TEXT_LABEL(mZeroFaile,   &gWD_Emission,  115, 0,  14, 14, 0, "×");
DEF_TEXT_LABEL(mCreepOk,   	 &gWD_Emission,  115, 16,  14, 14, 0, "OK");
DEF_TEXT_LABEL(mCreepFaile,  &gWD_Emission,  115, 16,  14, 14, 0, "×");
DEF_TEXT_LABEL(mBkZeroOk,  	 &gWD_Emission,  115, 32,  14, 14, 0, "OK");
DEF_TEXT_LABEL(mBkZeroFaile, &gWD_Emission,  115, 32,  14, 14, 0, "×");


DEF_TEXT_LABEL(msReslt,   	 &gWD_Emission, 139,  0,  32, 16, CTRL_VISABLE, "前档位:");		/* 上次分组 */
DEF_BIG_FLOAT_LABLE(mReslt,	 &gWD_Emission, 132, 16,  32, 16, CTRL_VISABLE, &Param.LastGroup,0,0,9999,"",TA_RIGHT);	/* 上次分组 */

DEF_TEXT_LABEL(msStep,   	 &gWD_Emission, 0, 48,  32, 16, CTRL_VISABLE, StrBuff[1]);		/* 本次分组 */
DEF_TEXT_LABEL(mOkCnt,   	 &gWD_Emission, 143, 49,  32, 16, CTRL_VISABLE, StrBuff[3]);	/* 成功次数 */

DEF_SMALL_FLOAT_LABLE(mgZero,&gWD_Emission, 49,  0, 28, 14, 1, &Param.RtZero, 	  4, -2.999999, 2.999999, "", TA_RIGHT);
DEF_SMALL_FLOAT_LABLE(mgOnly,&gWD_Emission, 49, 16, 28, 14, 1, &Param.RtSensitive,4, -2.999999, 5.999999, "", TA_RIGHT);
DEF_SMALL_FLOAT_LABLE(mgbkZr,&gWD_Emission, 49, 32, 28, 14, 1, &Param.RtBkZero,   4, -2.999999, 2.999999, "", TA_RIGHT);


static LPControl marrLPControl[] = 
{
	&mZero,
	&mbkzero,
	&mOnly,
	&msStep,
	&msReslt,
	&mReslt,
//	&mState,
	&mOkCnt,
	
	&mgZero,
	&mgOnly,
	&mgbkZr,
	
	&mZeroOk,   	
	&mZeroFaile,  
	&mCreepOk,   	
	&mCreepFaile, 
	&mBkZeroOk,  	
	&mBkZeroFaile,
	
};


CWindow  gWD_Emission = {
    marrLPControl,
	sizeof(marrLPControl)/sizeof(LPControl),
    0,
    0,
    0,
    192,
    64,
	WS_VISABLE,
    NULL,
    50,
    NULL,
    Form_Emission_Timer,
    Form_Emission_Draw,
    Form_Emission_Proc
};


void Form_Emission_Timer(void *ptmr)
{
	LPWindow lpWindow = (LPWindow)ptmr;
	GuiMsgInfo 	guiMsgInfo;					/* GUI消息结构实体 */
	
	if (lpWindow != NULL) {			

		g_pCurWindow 		= &gWD_Emission;	/* 设置主窗口 */				
		guiMsgInfo.pWindow 	= g_pCurWindow;		/* 向主当前窗口发送加载消息 */
		guiMsgInfo.ID 		= WM_TIMER;
		GuiMsgQueuePost(&guiMsgInfo);
		
	}
}


void Form_Emission_Draw(LPWindow pWindow)
{
	u16 		i;
	LPControl 	lpControl;							/* 子子控件结构指针 */
			
    SetColor(0);			
	EraseBuffer();									/* 清除显存 */
	SetRedraw(FALSE);								/* 禁止绘图	m_nRedrawIndex++; */
	EnableScreenFlush(FALSE);						/* 禁止刷屏	m_bScreenFlush = FALSE; */
	SetGdiView(	pWindow->nViewPosX, 			
				pWindow->nViewPosY, 			
				pWindow->nViewSizeX, 			
				pWindow->nViewSizeY);				/* 设置视图 */
	EnableGdiView(TRUE);							/* m_bUseView = TRUE */			

	for (i = 0; i < pWindow->nNbControls; i++) {	/* 绘制控件 */
		lpControl = *(pWindow->pLPControls + i);	/* 取当前控件 */
		if (lpControl->state & CTRL_VISABLE) {
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
			lpControl->DrawFunc(lpControl);
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
		}
	}
//    DrawHoriLine(0, 40, 190);
    DrawHoriLine(0, 47, 130);
    DrawVertLine(130, 0, 64);
	EnableScreenFlush(TRUE);						/* 使能刷屏 */
	FlushScreen();									/* 刷屏 */
	SetRedraw(TRUE);								/* 使能绘图 */
}

void Form_Emission_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo)
{
	switch (pGuiMsgInfo->ID) {
		case WM_LOAD:
			if (Param.bitNum == 0) {
				mgZeroCC.nFixPoint = 4;
				mgOnlyCC.nFixPoint = 4;
				mgbkZrCC.nFixPoint = 4;
			} else {
				mgZeroCC.nFixPoint = 3;
				mgOnlyCC.nFixPoint = 3;
				mgbkZrCC.nFixPoint = 3;	
			}
			rt_thread_delay(100);
			EraseScreen();
			msStep.state &= ~CTRL_VISABLE;
			mZeroOk.state 	= 0;
			mCreepOk.state 	= 0;
			mBkZeroOk.state = 0;
			mgOnly.state 	= 0;
			mgbkZr.state 	= 0;

			mZero.state 	= CTRL_VISABLE;
			mOnly.state 	= CTRL_VISABLE;
			mbkzero.state 	= CTRL_VISABLE;						
			mgZero.state 	= CTRL_VISABLE;
			mReslt.state	= CTRL_VISABLE;
			
			rt_thread_delay(15);
			DestoryWindowTimer(pWindow);
			pWindow->DrawFunc(pWindow);
			break;
		
		case WM_SHOW:	
			switch (AppState.BsnsFlow) {
				case BSNS_ZERO_DRIFT_OK:
					mZeroOk.state = CTRL_VISABLE;	/* 零点测试成功显示OK */
					mgOnly.state = CTRL_VISABLE;	/* 同时开始显示灵敏度的值 */
					break;
				case BSNS_CREEP_OK:					/* 蠕变测试成功显示ok */
					mCreepOk.state = CTRL_VISABLE;
					break;

				case BSNS_BKZR_FAILE:
					mgbkZr.state = CTRL_VISABLE;	/* 测试失败显示回零值 */
					break;
				
				case BSNS_BKZR_OK:
					msStep.state |= CTRL_VISABLE;	/* 回零测试成功,显示当前分组 */	
					mgbkZr.state = CTRL_VISABLE;	/* 测试成功显示回零值 */
					mBkZeroOk.state = CTRL_VISABLE;	/* 显示回零的“OK” */
					PostWindowMsg(pWindow, WM_LOAD, 0, 0);
					break;				
				
				case BSNS_FLOW_FAILE:
					if (tim_flag == 0) {
						tim_flag = 1;
						CreateWindowTimer(pWindow);	/* 5s创建退出时间,作为超时判断 */
						StartWindowTimer(pWindow);							
					}
					break;
					
				case BSNS_FLOW_WAIT:
				case BSNS_FLOW_START:
					if (tim_flag == 1) {
						tim_flag = 0;
						StopWindowTimer(pWindow);
						DestoryWindowTimer(pWindow);
						mZero.state 	= CTRL_VISABLE;
						mOnly.state 	= CTRL_VISABLE;
						mbkzero.state 	= CTRL_VISABLE;						
						mgZero.state 	= CTRL_VISABLE;
						mReslt.state	= CTRL_VISABLE;						
					}
					break;
			}
			sprintf(StrBuff[1], "本档位:%d", Param.RtGroup);
			sprintf(StrBuff[3], "%7d", Param.FlowOkCnt);
			pWindow->DrawFunc(pWindow);
			break;
			
		case WM_TIMER:
			if (AppState.isSnsr == SNSR_ON) {
				switch (AppState.LastBsnsFaile) {
					case BSNS_ZERO_FAILE:
					case BSNS_ZERO_DRIFT_FAILE:
						if (flash_flag == 0) {
							flash_flag = 1;
							mZero.state = 0;
							mgZero.state = 0;
							mReslt.state	= 0;
						} else {
							flash_flag = 0;
							mZero.state = CTRL_VISABLE;
							mgZero.state = CTRL_VISABLE;
							mReslt.state	= CTRL_VISABLE;
						}
						break;
					
					case BSNS_CREEP_FAILE:
					case BSNS_SNSTV_FAILE:
						if (flash_flag == 0) {
							flash_flag = 1;
							mOnly.state = 0;
							mgOnly.state = 0;
							mReslt.state	= 0;
						} else {
							flash_flag = 0;
							mOnly.state = CTRL_VISABLE;
							mgOnly.state = CTRL_VISABLE;
							mReslt.state	= CTRL_VISABLE;
						}					
						break;
					
					case BSNS_BKZR_FAILE:
						if (flash_flag == 0) {
							flash_flag = 1;
							mbkzero.state = 0;
							mgbkZr.state = 0;
							mReslt.state	= 0;
						} else {
							flash_flag = 0;
							mbkzero.state = CTRL_VISABLE;
							mgbkZr.state = CTRL_VISABLE;
							mReslt.state	= CTRL_VISABLE;
						}					
						break;
				}			
			} else {
				mZero.state 	= CTRL_VISABLE;
				mOnly.state 	= CTRL_VISABLE;
				mbkzero.state 	= CTRL_VISABLE;						
				mgZero.state 	= CTRL_VISABLE;	
				mReslt.state	= CTRL_VISABLE;				
			}

			
			break;
		
		case WM_FLOW:
			if (Param.bitNum == 0) {
				mgZeroCC.nFixPoint = 4;
				mgOnlyCC.nFixPoint = 4;
				mgbkZrCC.nFixPoint = 4;
			} else {
				mgZeroCC.nFixPoint = 3;
				mgOnlyCC.nFixPoint = 3;
				mgbkZrCC.nFixPoint = 3;	
			}
			//rt_thread_delay(100);
			EraseScreen();
			msStep.state &= ~CTRL_VISABLE;
			mZeroOk.state 	= 0;
			mCreepOk.state 	= 0;
			mBkZeroOk.state = 0;
			mgOnly.state 	= 0;
			mgbkZr.state 	= 0;

			mZero.state 	= CTRL_VISABLE;
			mOnly.state 	= CTRL_VISABLE;
			mbkzero.state 	= CTRL_VISABLE;						
			mgZero.state 	= CTRL_VISABLE;		
			
			rt_thread_delay(10);
			DestoryWindowTimer(pWindow);
			pWindow->DrawFunc(pWindow);			
			break;


		case WM_KEYDOWN: 
			pWindow->DrawFunc(pWindow);
			switch(pGuiMsgInfo->wParam) {	//按键值处理
				case KEY_MENU:
					DestoryWindowTimer(pWindow);
					g_pCurWindow = &gWD_MainMenu;
					g_pCurWindow->pParentWindow = &gWD_Emission;
					PostWindowMsg(g_pCurWindow, WM_LOAD,1,1);
					break;	
				
				case KEY_LEFT:
					AppState.TestSwitch = TEST_SWITCH_ON;
					break;
				
				case KEY_RIGHT:
					AppState.TestSwitch = TEST_SWITCH_OFF;
					break;
				
				case KEY_BACK:						/* 显示小数后三位还是四位 */	
					if (Param.bitNum == 0) {
						Param.bitNum = 1;
					} else {
						Param.bitNum = 0;
					}
					
					if (Param.bitNum == 0) {
						mgZeroCC.nFixPoint = 4;
						mgOnlyCC.nFixPoint = 4;
						mgbkZrCC.nFixPoint = 4;
					} else {
						mgZeroCC.nFixPoint = 3;
						mgOnlyCC.nFixPoint = 3;
						mgbkZrCC.nFixPoint = 3;	
					}
					Param_OutSideOpera(CC_BitNum, PARAM_CMD_WRITE);
					pWindow->DrawFunc(pWindow);
					break;
				
				default: 
					break;
			}
			break;

		default:			
			break;		

	}
}
