#include 	<stdio.h>
#include 	<string.h>
#include 	<stdlib.h>
#include	<math.h>

#include 	"stm32f10x.h"
#include 	<board.h>

#include	"AppParam.h"
#include	"AppState.h"

#include	"gdi.h"
#include	"guiwindows.h"
#include 	"TextEdit.h"

#include 	"bsp_key.h"


extern	CWindow  gWD_Celibration;

static void Form_Celibration_Timer(void *ptmr);
static void Form_Celibration_Draw(LPWindow pWindow);
static void Form_Celibration_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo);

static 	char	StrBuff[4][20];


DEF_TEXT_LABEL(msSysZero,	&gWD_Celibration, 0, 0,  32, 16, CTRL_VISABLE, "0mV/V:");


DEF_TEXT_LABEL(mgCaliVolt,	&gWD_Celibration, 0,  16, 28, 14, CTRL_VISABLE, StrBuff[0]);
DEF_TEXT_LABEL(mgSysZero, 	&gWD_Celibration, 70, 0, 28, 14, CTRL_VISABLE, StrBuff[1]);
DEF_TEXT_LABEL(mgSysLoad, 	&gWD_Celibration, 70, 16, 28, 14, CTRL_VISABLE, StrBuff[2]);

static LPControl marrLPControl[] = 
{
	&msSysZero,

	&mgCaliVolt,
	&mgSysZero,
	&mgSysLoad,
};

CWindow  gWD_Celibration = {
    marrLPControl,
	sizeof(marrLPControl)/sizeof(LPControl),
    0,
    0,
    0,
    192,
    64,
	WS_VISABLE,
    NULL,
    10,
    NULL,
    Form_Celibration_Timer,
    Form_Celibration_Draw,
    Form_Celibration_Proc
};


void Form_Celibration_Timer(void *ptmr)
{
	LPWindow lpWindow = (LPWindow)ptmr;
	
	if (lpWindow != NULL) {			

	}
}

void Form_Celibration_Draw(LPWindow pWindow)
{
	u16 		i;
	LPControl 	lpControl;							/* 子子控件结构指针 */
			
    SetColor(0);			
	EraseBuffer();									/* 清除显存 */
	SetRedraw(FALSE);								/* 禁止绘图	m_nRedrawIndex++; */
	EnableScreenFlush(FALSE);						/* 禁止刷屏	m_bScreenFlush = FALSE; */
	SetGdiView(	pWindow->nViewPosX, 			
				pWindow->nViewPosY, 			
				pWindow->nViewSizeX, 			
				pWindow->nViewSizeY);				/* 设置视图 */
	EnableGdiView(TRUE);							/* m_bUseView = TRUE */

	for (i = 0; i < pWindow->nNbControls; i++) {	/* 绘制控件 	*/
		lpControl = *(pWindow->pLPControls + i);	/* 取当前控件 	*/
		if (lpControl->state & CTRL_VISABLE) {
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
			lpControl->DrawFunc(lpControl);
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
		}
	}
	EnableScreenFlush(TRUE);						/* 使能刷屏 */
	FlushScreen();									/* 刷屏 */
	SetRedraw(TRUE);								/* 使能绘图 */
}

void Form_Celibration_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo)
{
	char	cTemp[20];
	
	switch (pGuiMsgInfo->ID) {
		case WM_LOAD:
			
			mgCaliVolt.state |= CTRL_FOCUS;
			mgSysZero.state  &= (~CTRL_FOCUS);
			mgSysLoad.state  &= (~CTRL_FOCUS);
		
			Param_OutSideOpera(CC_CaliVolt, PARAM_CMD_READ);
			Param_OutSideOpera(CC_CaliZero, PARAM_CMD_READ);
			Param_OutSideOpera(CC_CaliLoad, PARAM_CMD_READ);
			AppState.CaliFlow = CALIBRATION_NO;
		
		case WM_SHOW:	
			switch (AppState.CaliFlow) {
				case CALIBRATION_0mV:
					Param.CaliZero = (s32)pGuiMsgInfo->wParam;
					break;
				
				case CALIBRATION_20mV:
					Param.CaliLoad = (s32)pGuiMsgInfo->wParam;
					break;
			}
			sprintf(StrBuff[0], "%0.2fmV/V:",Param.CaliVolt);
			sprintf(StrBuff[1], "%d", 		Param.CaliZero);
			sprintf(StrBuff[2], "%d", 		Param.CaliLoad);			
			pWindow->DrawFunc(pWindow);
			break;

		case WM_KEYRepeat:
			break;
		
		case WM_KEYDOWN: 
			pWindow->DrawFunc(pWindow);
			switch(pGuiMsgInfo->wParam) {	//按键值处理
				case KEY_OK:
					memset(cTemp, 0, sizeof(cTemp));
					if (mgCaliVolt.state & CTRL_FOCUS) {
						if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 4)) {
							Param.CaliVolt = atof(cTemp);
							ParamLimitCheck(CC_CaliVolt);
							Param_OutSideOpera(CC_CaliVolt, PARAM_CMD_WRITE);
						}
                    } else if (mgSysZero.state & CTRL_FOCUS) {
                        mgSysZero.state &=~CTRL_FOCUS;
                        mgSysLoad.state |= CTRL_FOCUS;
						Param_OutSideOpera(CC_CaliZero, PARAM_CMD_WRITE);	/* 保存零点AD	*/
						AppState.CaliFlow = CALIBRATION_20mV;				/* 开始表20mV		*/
                    } else if(mgSysLoad.state & CTRL_FOCUS) {
                        mgSysLoad.state &=~CTRL_FOCUS;
                        mgCaliVolt.state |= CTRL_FOCUS;
						Param_OutSideOpera(CC_CaliLoad, PARAM_CMD_WRITE);	/* 保存满度AD	*/
						AppState.state = SYS_RUN;
						AppState.CaliFlow = CALIBRATION_NO;
                    }
					break;	
				
				case KEY_LEFT:
					
					break;
				
				case KEY_RIGHT:
					if (mgCaliVolt.state & CTRL_FOCUS) {
                        mgCaliVolt.state &=~CTRL_FOCUS;
                        mgSysZero.state |= CTRL_FOCUS;
						AppState.CaliFlow = CALIBRATION_0mV;				/* 开始表0mV		*/
						AppState.state = SYS_CALIBRATION;
                    } else if (mgSysZero.state & CTRL_FOCUS) {
                        mgSysZero.state &=~CTRL_FOCUS;
                        mgSysLoad.state |= CTRL_FOCUS;

						AppState.CaliFlow = CALIBRATION_20mV;				/* 开始表20mV		*/
						Param_OutSideOpera(CC_CaliZero, PARAM_CMD_READ);
                    } else if(mgSysLoad.state & CTRL_FOCUS) {
                        mgSysLoad.state &=~CTRL_FOCUS;
                        mgCaliVolt.state |= CTRL_FOCUS;
						
						AppState.state	  = SYS_RUN;
						AppState.CaliFlow = CALIBRATION_NO;
						Param_OutSideOpera(CC_CaliLoad, PARAM_CMD_READ);
                    }
					break;

				case KEY_BACK:
					Param_OutSideOpera(CC_CaliVolt, PARAM_CMD_READ);
					Param_OutSideOpera(CC_CaliZero, PARAM_CMD_READ);
					Param_OutSideOpera(CC_CaliLoad, PARAM_CMD_READ);
					AppState.state = SYS_RUN;								/* 切换到运行状态 */
					AppState.CaliFlow = CALIBRATION_NO;
					if (pWindow->pParentWindow != NULL) {
						g_pCurWindow = pWindow->pParentWindow;
						PostWindowMsg(g_pCurWindow, WM_LOAD, 0, 0);
					}
					break;				
				
				default: 
					break;
			}
			break;

		default:			
			break;		

	}
}
