#include 	<stdio.h>
#include 	"string.h"
#include 	"stm32f10x.h"
#include 	<board.h>

#include	"gdi.h"
#include	"guiwindows.h"
#include 	"TextEdit.h"
#include 	"Form_MsgBox.h"
#include	"menu.h"
#include 	"bsp_key.h"


extern	CWindow  	gWD_Emission;
extern	CWindow  	gWD_SetFun;
extern	CWindow  	gWD_TestFun;
extern	CWindow  	gWD_Celibration;
extern	CWindow  	gWD_SysParam;

extern void FormMainMenuProc(struct _CWindow* pParentWindow, u32 nCmd);
extern void FormDrvMainMenuProc(struct _CWindow* pParentWindow, u32 nCmd);

/*
 *  主菜单项定义
 */
DEF_MENU_ITEM(mnQuery,	    	"范围设置",    	0, 		NULL);
DEF_MENU_ITEM(mnPrintSet, 		"测试条件", 	1, 		NULL);
DEF_MENU_ITEM(mnCalibSet,		"本机标定",    	2, 		NULL);
DEF_MENU_ITEM(mnSysSet,			"测试时间", 	3, 		NULL);



static LPMenuItem m_MainMenuItems[] = 
{
	&mnQuery,			/* 系统查询 */ 	
	&mnPrintSet,	  	
	&mnCalibSet,
	&mnSysSet,
};


DEF_MENU(mMainMenu, "主菜单", m_MainMenuItems, NULL, FormMainMenuProc);	 
DEF_MENU_WND(gWD_MainMenu, &mMainMenu, &gWD_Emission);




void FormMainMenuProc(struct _CWindow* pParentWindow, u32 nCmd)
{	
	char 	cbuf[10]="";
	u8 		ret = 0;
	
	switch (nCmd) {
		case 0:
			g_pCurWindow = &gWD_SetFun;							
			g_pCurWindow->pParentWindow = &gWD_MainMenu;
			PostWindowMsg(g_pCurWindow, WM_LOAD, NULL, NULL);			
			break;
		
		case 1:
			g_pCurWindow = &gWD_TestFun;						
			g_pCurWindow->pParentWindow = &gWD_MainMenu;
			PostWindowMsg(g_pCurWindow, WM_LOAD, NULL, NULL);			
			break;
		
		case 2:
			if ( LoadEditDlg(&g_TextEditWindow, "输入密码", cbuf, cbuf, 4)) {
				if (0 == strcmp(cbuf,"1891") ) {
					ret = 1;
				} 
				if (0 == strcmp(cbuf,"1111")) {
					ret = 2;
				}
			} else {
				ret = 0xff;
			}
			if (ret == 0xff) break;
			if (ret == 0) {
				MsgBoxDlg(&g_MsgBoxDlg, "错误", "密码错误");
				break;
			}		
			g_pCurWindow = &gWD_Celibration;					
			g_pCurWindow->pParentWindow = &gWD_MainMenu;
			PostWindowMsg(g_pCurWindow, WM_LOAD, NULL, NULL);				
			break;
		
		case 3:
			g_pCurWindow = &gWD_SysParam;						
			g_pCurWindow->pParentWindow = &gWD_MainMenu;
			PostWindowMsg(g_pCurWindow, WM_LOAD, NULL, NULL);		
			break;
		
		default:
			break;
	}
}

