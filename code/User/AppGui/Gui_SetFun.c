#include 	<stdio.h>
#include 	<string.h>
#include 	<stdlib.h>
#include	<math.h>

#include 	"stm32f10x.h"
#include 	<board.h>
#include	"AppParam.h"
#include	"common.h"
#include	"gdi.h"
#include	"guiwindows.h"
#include 	"TextEdit.h"
#include 	"bsp_key.h"

extern	CWindow  gWD_SetFun;

static void Form_SetFun_Timer(void *ptmr);
static void Form_SetFun_Draw(LPWindow pWindow);
static void Form_SetFun_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo);

static	char	sGroup[10];

DEF_TEXT_LABEL(msSensiRange,&gWD_SetFun, 0,  0,  32, 16, CTRL_VISABLE, "灵敏度范围:");
DEF_TEXT_LABEL(msZeroRange,	&gWD_SetFun, 0, 16,  32, 16, CTRL_VISABLE, "零点范围: ");
DEF_TEXT_LABEL(msGearSize, 	&gWD_SetFun, 0, 32,  32, 16, CTRL_VISABLE, "分档值:");
DEF_TEXT_LABEL(msGearN,   	&gWD_SetFun, 0, 48,  32, 16, CTRL_VISABLE, "分档数:");

DEF_SMALL_FLOAT_LABLE(msetNetLdo,  &gWD_SetFun,  77, 0, 42, 14, CTRL_VISABLE, &Param.NetLdo, 4, -9.9999, 9.9999, "", TA_RIGHT);
DEF_SMALL_FLOAT_LABLE(msetNetLup,  &gWD_SetFun, 133, 0, 42, 14, CTRL_VISABLE, &Param.NetLup, 4, -9.9999, 9.9999, "", TA_RIGHT);

DEF_SMALL_FLOAT_LABLE(msetZeroLdo, &gWD_SetFun,  77, 16, 42, 14, CTRL_VISABLE, &Param.ZeroLdo, 4, -9.9999, 9.9999, "", TA_LEFT);
DEF_SMALL_FLOAT_LABLE(msetZeroLup, &gWD_SetFun, 133, 16, 42, 14, CTRL_VISABLE, &Param.ZeroLup, 4, -9.9999, 9.9999, "", TA_RIGHT);

DEF_SMALL_FLOAT_LABLE(msetStep,    &gWD_SetFun, 77, 32, 42, 14, CTRL_VISABLE, &Param.Step, 	4, -9.9999, 9.9999, "", TA_RIGHT);
DEF_TEXT_LABEL(msetGroup,   	   &gWD_SetFun, 77, 48, 35, 16, CTRL_VISABLE, sGroup);


static LPControl marrLPControl[] = 
{
	&msZeroRange,
	&msSensiRange,
	&msGearSize,
	&msGearN,
	
	&msetZeroLdo,
	&msetZeroLup,
	&msetNetLdo,
	&msetNetLup,
	&msetStep,
	&msetGroup,
};


CWindow  gWD_SetFun = {
    marrLPControl,
	sizeof(marrLPControl)/sizeof(LPControl),
    0,
    0,
    0,
    192,
    64,
	WS_VISABLE,
    NULL,
    10,
    NULL,
    Form_SetFun_Timer,
    Form_SetFun_Draw,
    Form_SetFun_Proc
};


void Form_SetFun_Timer(void *ptmr)
{
	LPWindow lpWindow = (LPWindow)ptmr;
	
	if (lpWindow != NULL) {			

	}
}

void Form_SetFun_Draw(LPWindow pWindow)
{
	u16 		i;
	LPControl 	lpControl;				/* 子子控件结构指针 */

    SetColor(0);
	EraseBuffer();						//清除显存
	SetRedraw(FALSE);					//禁止绘图	m_nRedrawIndex++;
	EnableScreenFlush(FALSE);			//禁止刷屏	m_bScreenFlush = FALSE;
	SetGdiView(	pWindow->nViewPosX, 
				pWindow->nViewPosY, 
				pWindow->nViewSizeX, 
				pWindow->nViewSizeY);	//设置视图
	EnableGdiView(TRUE);				//			m_bUseView = TRUE

	//绘制控件
	for (i = 0; i < pWindow->nNbControls; i++) {
		lpControl = *(pWindow->pLPControls + i);	/* 取当前控件 */
		if (lpControl->state & CTRL_VISABLE) {
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
			lpControl->DrawFunc(lpControl);
            if (lpControl->state & CTRL_FOCUS) {
                InvertColor();
			}
		}
	}
	EnableScreenFlush(TRUE);			//使能刷屏
	FlushScreen();						//刷屏
	SetRedraw(TRUE);					//使能绘图
}




void Form_SetFun_Proc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo)
{
	CControl* 	pControl;	
	char		cTemp[20];
	float		net_aver;

	switch (pGuiMsgInfo->ID) {
		case WM_LOAD:
			msetNetLdo.state  |= ( CTRL_FOCUS);
			msetNetLup.state  &= (~CTRL_FOCUS);
			msetZeroLdo.state &= (~CTRL_FOCUS);
			msetZeroLup.state &= (~CTRL_FOCUS);
			msetStep.state    &= (~CTRL_FOCUS);
			msetGroup.state   &= (~CTRL_FOCUS);
			
		case WM_SHOW:			
			sprintf(sGroup, "%04d", Param.Group);
			pWindow->DrawFunc(pWindow);
			break;
		
		case WM_FLOW:
			pWindow->DrawFunc(pWindow);			
			break;

		case WM_UPDATECTRL:
			pControl = (CControl *)(pGuiMsgInfo->wParam);
			if (pControl != NULL) {
				pControl->DrawFunc(pControl);				/* 显示 */
			} 			
			break;

		case WM_TIMEUPDATE:
// 			PostWindowMsg(pWindow, WM_UPDATECTRL, (uint32)&gStatusBar, 0);
			break;
		
		case WM_KEYRepeat:
		case WM_KEYDOWN: 
			pWindow->DrawFunc(pWindow);
			switch(pGuiMsgInfo->wParam) {	//按键值处理
				case KEY_OK:
					memset(cTemp, 0, sizeof(cTemp));
					if (msetZeroLdo.state & CTRL_FOCUS) {
						if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 8)) {
							Param.ZeroLdo = atof(cTemp);
							net_aver = PRESENT_CUT(Param.NetLdo + (Param.NetLup - Param.NetLdo)*1, 100);
							if (Param.ZeroLdo < (-1 * net_aver)) {
								Param.ZeroLdo = (-1 * net_aver);
							}
							Param_OutSideOpera(CC_ZeroLdo, PARAM_CMD_WRITE);
						}
                    } else if(msetZeroLup.state & CTRL_FOCUS) {
						if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 8)) {
							Param.ZeroLup = atof(cTemp);
							net_aver = PRESENT_CUT(Param.NetLdo + (Param.NetLup - Param.NetLdo)*1, 100);	
							if (Param.ZeroLup > net_aver) {
								Param.ZeroLup = net_aver;
							}
							Param_OutSideOpera(CC_ZeroLup, PARAM_CMD_WRITE);
						}
                    } else if(msetNetLdo.state & CTRL_FOCUS) {
                        if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 8)) {
							Param.NetLdo = atof(cTemp);
							ParamLimitCheck(CC_NetLdo);
							Param.Group = Group_Cal(Param.NetLup, Param.NetLdo, Param.Step);
							Param_OutSideOpera(CC_NetLdo, PARAM_CMD_WRITE);
							Param_OutSideOpera(CC_Group, PARAM_CMD_WRITE);
						}
                    } else if(msetNetLup.state & CTRL_FOCUS) {
                        if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 8)) {
							Param.NetLup = atof(cTemp);
							ParamLimitCheck(CC_NetLup);
							Param.Group = Group_Cal(Param.NetLup, Param.NetLdo, Param.Step);
							Param_OutSideOpera(CC_NetLup, PARAM_CMD_WRITE);
							Param_OutSideOpera(CC_Group, PARAM_CMD_WRITE);
						}
                    } else if(msetStep.state & CTRL_FOCUS) {
                        if (LoadEditDlg(&g_TextEditWindow, "Input", cTemp, cTemp, 8)) {
							Param.Step = atof(cTemp);
							Param.Group = Group_Cal(Param.NetLup, Param.NetLdo, Param.Step);
							Param_OutSideOpera(CC_Step, PARAM_CMD_WRITE);
							Param_OutSideOpera(CC_Group, PARAM_CMD_WRITE);
						}
                    }
					break;	
				
				case KEY_LEFT:
					if(msetStep.state & CTRL_FOCUS) {
                        msetStep.state &=~CTRL_FOCUS;
                        msetZeroLup.state |= CTRL_FOCUS;
                    } else if(msetZeroLup.state & CTRL_FOCUS) {
                        msetZeroLup.state &=~CTRL_FOCUS;
                        msetZeroLdo.state |= CTRL_FOCUS;
                    } else if (msetZeroLdo.state & CTRL_FOCUS) {
                        msetZeroLdo.state &=~CTRL_FOCUS;
                        msetNetLup.state |= CTRL_FOCUS;
                    } else if(msetNetLup.state & CTRL_FOCUS) {
                        msetNetLup.state &=~CTRL_FOCUS;
                        msetNetLdo.state |= CTRL_FOCUS;
                    }else if(msetNetLdo.state & CTRL_FOCUS) {
                        msetNetLdo.state &=~CTRL_FOCUS;
                        msetStep.state |= CTRL_FOCUS;
                    }
					break;
				
				case KEY_RIGHT:
					if (msetNetLdo.state & CTRL_FOCUS) {
                        msetNetLdo.state &=~CTRL_FOCUS;
                        msetNetLup.state |= CTRL_FOCUS;
                    } else if(msetNetLup.state & CTRL_FOCUS) {
                        msetNetLup.state &=~CTRL_FOCUS;
                        msetZeroLdo.state |= CTRL_FOCUS;
                    } else if (msetZeroLdo.state & CTRL_FOCUS) {
                        msetZeroLdo.state &=~CTRL_FOCUS;
                        msetZeroLup.state |= CTRL_FOCUS;
                    } else if(msetZeroLup.state & CTRL_FOCUS) {
                        msetZeroLup.state &=~CTRL_FOCUS;
                        msetStep.state |= CTRL_FOCUS;
                    } else if(msetStep.state & CTRL_FOCUS) {
                        msetStep.state &=~CTRL_FOCUS;
                        msetNetLdo.state |= CTRL_FOCUS;
                    }
					break;

				case KEY_BACK:
					Param_OutSideOpera(CC_ZeroLdo, PARAM_CMD_READ);
					Param_OutSideOpera(CC_ZeroLup, PARAM_CMD_READ);
					Param_OutSideOpera(CC_NetLdo, PARAM_CMD_READ);
					Param_OutSideOpera(CC_NetLdo, PARAM_CMD_READ);
					Param_OutSideOpera(CC_Step, PARAM_CMD_READ);
					Param_OutSideOpera(CC_Group, PARAM_CMD_READ);
					if (pWindow->pParentWindow != NULL) {
						g_pCurWindow = pWindow->pParentWindow;
						PostWindowMsg(g_pCurWindow, WM_LOAD,0,0);
					}
					break;				
				
				default: 
					break;
			}
			break;

		default:			
			break;		

	}
}
