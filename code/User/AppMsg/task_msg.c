/*
 *	文件说明
 * 	History:
 * 		1. 2012-12-24	修改消息队列发送数据队列维护异常的bug!
 * 		2. 2013-02-05	重新封装系统消息队列
 */
/*
 **********************************************************************************
 *								头文件包含区
 **********************************************************************************
 */
#include 	"stm32f10x.h"
#include 	<rtthread.h>
#include	"task_msg.h"


/*
 **********************************************************************************
 *								函数定义区
 **********************************************************************************
 */
/**********************************************************************************
 *	作用：任务消息创建
 *		*pMsgQ		消息队列
 *		**pSysQ		消息队列的缓存
 *		*pMem		消息队列所需要的内存池
 *		nMsgCount	消息队列深度
 *	
 **********************************************************************************/
u8 TaskMsg_Create(MsgQ *pMsgQ, char *name, u32 len)
{
	u8	err = 0;
	
	pMsgQ->pEvent = rt_mq_create(name, sizeof(TaskMsgDef), len, RT_IPC_FLAG_FIFO);
	
	return err;
}
/*
 *	
 */
u8 Task_QPostFront(MsgQ *pMsgQ, void *pMsg)
{
	s8 err;

	return err;
}

/*
 *	队列发送数据
 */
u8 Task_QPost(MsgQ *pMsgQ, void *pMsg)
{
	u8 err;

	err = rt_mq_send(pMsgQ->pEvent, pMsg, sizeof(TaskMsgDef));	

	return err;
}

/*
 *	等待队列同步
 */
u8 Task_QPend(MsgQ *pMsgQ, void *pMsg, int len, s16 timeout)
{
	u8 err;	
	
	err = rt_mq_recv(pMsgQ->pEvent, pMsg, len, RT_WAITING_FOREVER);

	return err;
}
/*
 **********************************************************************************
 *								end
 **********************************************************************************
 */
