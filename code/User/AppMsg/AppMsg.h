



/*
 *	通信参数
 */
typedef struct {
	u8 		CmdID;				/* 要执行的参数操作，如写参数，读参数，主动上传参数等 */
	u8		AckID;				/* 发出命令的来源 */
	u8		Len;				/* 数据长度 */
	u8		PacketID;			/* 组合消息的编号 */
	u8		PacketItemID;		/* 某个参数在组合消息中的段号 */
	u8		DeviceID;			/* 参数所在的设备 */
	u8		ParamID;			/* 某个参数的编号 */
	u8		ReturnState;		/* 读参数时，返回读参数成功或失败 */
	u8		value[32];			/* 参数值 */
}TaskParamMsgDef;



extern	MsgQ	MsgQ_Business;

extern	void UserMsg_Init(void);


