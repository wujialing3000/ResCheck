#include "guiwindows.h"
#include "string.h"
#include "stdio.h"

#include <rtthread.h>


#define		MSG_SIZE	sizeof(GuiMsgInfo)
#define		MSG_LEN		20

#define 	MsgQSize 	sizeof(TaskMsg)

CWindow*		g_pCurWindow;

u8				GuiMsgInfoStart;					//消息起始索引
u8				GuiMsgInfoEnd;						//消息停止索引


//rt_mailbox_t	m_pEvtGuiMsg = RT_NULL;
rt_mq_t			m_pEvtGuiMsg = RT_NULL;			/* 使用队列接收到的全是0 */

/*
 *	绘制默认窗口
 */
void DefaultWindowDraw(LPWindow pWindow)
{
	u16 i;
	LPControl	lpControl;

	
	EraseBuffer();									/* 清除显存 		*/
	SetRedraw(FALSE);								/* 禁止绘图 		*/
	EnableScreenFlush(FALSE);						/* 禁止刷屏 		*/
	SetGdiView(	pWindow->nViewPosX, 			
				pWindow->nViewPosY, 			
				pWindow->nViewSizeX, 			
				pWindow->nViewSizeY);				/* 设置视图 		*/
	EnableGdiView(TRUE);

	for (i = 0; i < pWindow->nNbControls; i++) {	/* 绘制控件 		*/
		lpControl = *(pWindow->pLPControls + i);
		if (lpControl->state & CTRL_VISABLE) {
			lpControl->DrawFunc(lpControl);
		}
	}

	EnableScreenFlush(TRUE);						/* 使能刷屏 		*/
	FlushScreen();									/* 刷屏 			*/
	SetRedraw(TRUE);								/* 使能绘图 		*/
}

void DefaultWindowProc(LPWindow pWindow, LPGuiMsgInfo pGuiMsgInfo)
{
	
}
/*
 *	创建一个窗口定时器
 */
u8 CreateWindowTimer(LPWindow lpWindow)
{
	u8 err;

	if (lpWindow->tmrCallBack == NULL) return FALSE;
	if (lpWindow->pTimer != NULL) return TRUE;

	
	lpWindow->pTimer = rt_timer_create(	"windows timer", 			/* 定时器的名称 					*/
										lpWindow->tmrCallBack,		/* 定时器超时函数指针 				*/
										lpWindow,					/* 定时器超时函数的入口参数 		*/
										lpWindow->period,			/* 定时器的超时时间，单位是系统节拍 */	
										RT_TIMER_FLAG_PERIODIC);	/* 周期定时 						*/
	
	if (err == SYS_ERR_NONE) {
		return TRUE;
	}
	return FALSE;	
}

/*
 *	创建一个窗口定时器
 */
u8 DestoryWindowTimer(LPWindow lpWindow)
{
	u8 bResult;

	if (lpWindow->pTimer == NULL)  return TRUE;	
			
	bResult = rt_timer_delete(lpWindow->pTimer);
	lpWindow->pTimer = NULL;

	return bResult;
}
/*
 *	启动窗口定时器
 */
u8 StartWindowTimer(LPWindow lpWindow)
{
	u8 err;
	
	if (lpWindow->pTimer == NULL)  return FALSE;

	err = rt_timer_start(lpWindow->pTimer);
	
	return	err;
}
/*
 *	停止窗口定时器
 */
u8 StopWindowTimer(LPWindow lpWindow)
{
	u8 err;
	
	if (lpWindow->pTimer == NULL) return FALSE;
	
	err = rt_timer_stop(lpWindow->pTimer);
	
	return	err;
}


void DrawTextLabel(CControl* pControl)
{
	int nStrSize = 0;
	CTextLabel* pTextLabel;
	char*	s;
	pTextLabel =  (CTextLabel*)(pControl->pContent);
	s = pTextLabel->s;

	if(s != NULL)
	{
		nStrSize = strlen((char*)s);	
		if(nStrSize > 0)
		{		
			DrawGbText((char*)s, pControl->x, pControl->y);
			//DrawGbText(":", nStrSize*8, 0);
		}	
	}
}

void DrawSmallFloatLabel(CControl* pControl)
{
	s16 wText;
	char s[16], sPoint[10];

	CFloatLabel* pLabel = (CFloatLabel*)(pControl->pContent);

	//清除控件区域
	EnableScreenFlush(FALSE);

//	SetColor(1);	/* 去掉，这样可以使用选中某个数字反白显示 */
	FillRect(pControl->x, pControl->y, pControl->sx, pControl->sy);
//	SetColor(0);

	if ((pControl->state & CTRL_VISABLE) == 0) {
		sprintf(s, "");
	} else {
		if( *pLabel->pF > pLabel->max) {
			sprintf(s, "----");
		} else if(*pLabel->pF < pLabel->min) {
			sprintf(s, "----");
		} else {
			sprintf(sPoint, "%%.%df%s", pLabel->nFixPoint, pLabel->unit);
			sprintf(s, sPoint, *pLabel->pF);
		}
	}
	wText = strlen(s)*7;
	if(pControl->sx >= wText) {			//控件宽度大于文本宽度
	
		if ((pControl->align & 0x000F) == TA_LEFT) {
			DrawGbText(s, pControl->x, pControl->y); 
		} else if((pControl->align & 0x000F) == TA_CENTER) {
			DrawGbText(s, pControl->x + (pControl->sx - wText)/2, pControl->y); 
		} else if((pControl->align & 0x000F) == TA_RIGHT) {
			DrawGbText(s, pControl->x + (pControl->sx - wText), pControl->y); 
		}	
	} else {							//左对齐
		DrawGbText(s, pControl->x, pControl->y);
		//DrawSmallText(s, pControl->x, pControl->y);
	}
	//刷新控件区域
	EnableScreenFlush(TRUE);
	FlushRect(pControl->x, pControl->y, pControl->sx, pControl->sy);
	//FlushScreen();
}

void DrawBigFloatLabel(CControl* pControl)
{
	s16 wText;
	char s[16], sPoint[10];
	CFloatLabel* pLabel = (CFloatLabel*)(pControl->pContent);

	//清除控件区域
	EnableScreenFlush(FALSE);

	SetColor(1);
	FillRect(pControl->x, pControl->y, pControl->sx, pControl->sy);
	SetColor(0);

	if((*pLabel->pF > pLabel->max) || (*pLabel->pF < pLabel->min))
	{
		sprintf(s, "----");
	}
	else
	{
		sprintf(sPoint, "%%.%df%s", pLabel->nFixPoint, pLabel->unit);
		sprintf(s, sPoint, *pLabel->pF);
	}

	wText = strlen(s)*15;
	if(pControl->sx >= wText)			//控件宽度大于文本宽度
	{
		if((pControl->align & 0x000F) == TA_LEFT)
		{
			DrawBigText(s, pControl->x, pControl->y); 
		}
		else if((pControl->align & 0x000F) == TA_CENTER)
		{
			DrawBigText(s, pControl->x + (pControl->sx - wText)/2, pControl->y); 
		}
		else if((pControl->align & 0x000F) == TA_RIGHT)
		{
			DrawBigText(s, pControl->x + (pControl->sx - wText), pControl->y); 
		}
		
	}
	else							//左对齐
	{
		DrawBigText(s, pControl->x, pControl->y);
	}

	//刷新控件区域
	EnableScreenFlush(TRUE);
	FlushRect(pControl->x, pControl->y, pControl->sx, pControl->sy);
}

void DrawStatusImage(CControl* pControl)
{
	CStatusImage* pImg = (CStatusImage*)(pControl->pContent);
	if(pImg->bHot == TRUE)
	{
		DrawImage(pImg->pHotImage, pControl->x, pControl->y, pImg->sx, pImg->sy);
	}
	else
	{
		DrawImage(pImg->pNormalImage, pControl->x, pControl->y, pImg->sx, pImg->sy);
	}
}

void DrawImageCtrl(CControl* pControl)
{
	CImageCtrl* pImg = (CImageCtrl*)(pControl->pContent);
 	DrawImage(pImg->pImage, pControl->x, pControl->y, pImg->sx, pImg->sy);	
	
//	LCD_ShowBMP(pControl->x, pControl->y, pImg->sx, pImg->sy, pImg->pImage);
}

void DrawCustomTextCtrl(CControl* pControl)
{	
	u16 i;
	CCustomTextCtrl* pText = (CCustomTextCtrl*)(pControl->pContent);
	for(i = 0; i< pText->nCount; i++)
	{
		//暂不支持换行
		DrawImage(pText->pArr + i*pText->nWordSize, pControl->x + i*pText->sx, pControl->y, pText->sx, pText->sy);			
	}	
}

void DrawRectCtrl(CControl* pControl)
{
	CRectCtrl* pRect = (CRectCtrl*)(pControl->pContent);
	DrawRect(pControl->x, pControl->y, pRect->cx, pRect->cy);	
}

u8 CreateCartoonCtrl(LPCartoonCtrl lpCartoonCtrl)
{
	u8 err;

	if(lpCartoonCtrl->tmrCallBack == NULL) return FALSE;
	if(lpCartoonCtrl->pTimer != NULL) return TRUE;

	lpCartoonCtrl->pTimer = rt_timer_create("windows timer", 			/* 定时器的名称 					*/
											lpCartoonCtrl->tmrCallBack,	/* 定时器超时函数指针 				*/
											lpCartoonCtrl,				/* 定时器超时函数的入口参数 		*/
											lpCartoonCtrl->period,		/* 定时器的超时时间，单位是系统节拍 */	
											RT_TIMER_FLAG_PERIODIC);	/* 周期定时 						*/
	
	if(err == SYS_ERR_NONE)
	{
		return TRUE;
	}
	return FALSE;
}

u8 DestoryCartoonCtrl(LPCartoonCtrl lpCartoonCtrl)
{
	u8 err;
	
	if (lpCartoonCtrl->pTimer == NULL)  return TRUE;	
		
	err = rt_timer_delete(lpCartoonCtrl->pTimer);
	
	return	err;
}

u8 StartCartoonCtrl(LPCartoonCtrl lpCartoonCtrl)
{
	u8 err;
	
	if (lpCartoonCtrl->pTimer == NULL) return FALSE;

	err = rt_timer_start(lpCartoonCtrl->pTimer);
	
	return	err;
}

u8 StopCartoonCtrl(LPCartoonCtrl lpCartoonCtrl)
{
	u8 bResult;

	if (lpCartoonCtrl->pTimer == NULL) return TRUE;

	bResult = rt_timer_stop(lpCartoonCtrl->pTimer);
	
	return bResult;
}


void CartoonTmrCallBack(void *ptmr, void *parg)
{
	GuiMsgInfo msg;
	LPCartoonCtrl lpCartoonCtrl = (LPCartoonCtrl)parg;
	
	if(lpCartoonCtrl->cartoonState == CARTOON_STATE_ALTER)
	{
		if(lpCartoonCtrl->pControl->pParent == g_pCurWindow)
		{
			lpCartoonCtrl->cartoonState = CARTOON_STATE_ALTER;
			lpCartoonCtrl->nFrame++;
			if(lpCartoonCtrl->nFrame > 1) lpCartoonCtrl->nFrame = 0;

			msg.ID = WM_UPDATECTRL;

			//测试窗体移动
//			g_pCurWindow->nViewPosX += 11;
//			g_pCurWindow->nViewPosY += 9;	
//			if(g_pCurWindow->nViewPosX > 128) g_pCurWindow->nViewPosX = -128;
//			if(g_pCurWindow->nViewPosY > 64) g_pCurWindow->nViewPosY = -64;
//			msg.ID = WM_SHOW;
			
			//测试窗体完
			msg.pWindow = g_pCurWindow;
			msg.wParam = (u32)(lpCartoonCtrl->pControl);
			GuiMsgQueuePost(&msg);
		}
	}
}

void DrawCartoonCtrl(CControl* pControl)
{
	CCartoonCtrl* pCartoon = (CCartoonCtrl*)(pControl->pContent);
	if(pCartoon->cartoonState == CARTOON_STATE_NORMAL)
	{
		DrawImage(pCartoon->pNormalImage, pControl->x, pControl->y, pCartoon->sx, pCartoon->sy);
	}
	else if(pCartoon->cartoonState == CARTOON_STATE_HOT)
	{
		DrawImage(pCartoon->pHotImage, pControl->x, pControl->y, pCartoon->sx, pCartoon->sy);
	}
	else if(pCartoon->cartoonState == CARTOON_STATE_ALTER)
	{
		if(pCartoon->nFrame == 0)
		{
			DrawImage(pCartoon->pNormalImage, pControl->x, pControl->y, pCartoon->sx, pCartoon->sy);
		}
		else
		{
			DrawImage(pCartoon->pHotImage, pControl->x, pControl->y, pCartoon->sx, pCartoon->sy);
		} 
	}		
}


/*
 *	初始化消息
 */
void GuiMsgQueueCreate(void)
{	
	m_pEvtGuiMsg = rt_mq_create("gui_queue", MSG_SIZE, MSG_LEN, RT_IPC_FLAG_FIFO);
}
/*
 *	发送消息
 */
u8 GuiMsgQueuePost(GuiMsgInfo* pGuiMsgInfo)
{
	
	return rt_mq_send(m_pEvtGuiMsg, pGuiMsgInfo, MSG_SIZE);	
}
/*
 *	等待消息
 */
void* GuiMsgQueuePend(GuiMsgInfo* pGuiMsgInfo, u8* err)
{
	
	*err = rt_mq_recv(m_pEvtGuiMsg, pGuiMsgInfo, MSG_SIZE, RT_WAITING_FOREVER);	//永远等待
		
	return NULL;
}

/*
 *	发送窗口消息
 */
u8 PostWindowMsg(LPWindow pWindow, u16 nID, u32 wParam, u32 lParam)
{
	GuiMsgInfo msg;

	msg.pWindow = pWindow;
	msg.ID 		= nID;
	msg.wParam 	= wParam;
	msg.lParam 	= lParam;

	return GuiMsgQueuePost(&msg);	
}



